import rv.commands as rvc
from PySide2 import QtCore

from .qt_utils import SingletonMixin


class RvPlaylist(SingletonMixin):
    app = None
    dialog = None

    def __init__(self, app):
        from .signalling import Signalling
        from .data_loader import DataLoader
        from .rv_session.rv_session import RvSession

        RvPlaylist.app = app
        self.set_signalling_obj(Signalling.get())
        self.data_loader = DataLoader()
        self.rv_session = RvSession()

    @classmethod
    def show_main_dlg(cls, app):
        cls._singleton = cls(app)
        cls._singleton._show_dlg()

    @classmethod
    def rv_playlist(cls):
        if not cls._singleton:
            return None
        return cls._singleton

    def _show_dlg(self):
        from .playlist_form import PlaylistForm
        from . import commands

        try:
            self.dialog = RvPlaylist.app.engine.show_panel(
                self.app._unique_panel_id,
                "Playlist",
                RvPlaylist.app,
                PlaylistForm,
                QtCore.Qt.RightDockWidgetArea,
            )

            commands.bind_hotkeys(self.app.hotkey_data)

        except:
            RvPlaylist.app.log_exception("Failed to create RV Playlist!")

    def open_file(self, paths):
        if isinstance(paths, str):
            paths = [paths]
        from .signalling import Signalling

        Signalling.get().load_files.emit(paths)

    def shut_down(self):
        self.app.log_debug("RV Playlist shut down")
        self.rv_session.shut_down()
        super().shut_down()
        RvPlaylist.app = None
        RvPlaylist.dialog = None
