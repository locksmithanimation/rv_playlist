import time
import operator
from pathlib import Path

from tank.platform.qt5 import QtCore, QtWidgets
import rv.commands as rvc

from .signalling import Signalling
from .rv_playlist import RvPlaylist
from .ui.attribute_widget import Ui_AttributeWidget
from .cut_list.cut_model import ItemDataRole


class AttributeWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(AttributeWidget, self).__init__(parent)
        self._ui = Ui_AttributeWidget()
        self._ui.setupUi(self)

        self._uid = None

        self._ui.shot_edt.editingFinished.connect(self.update_shot)
        self._ui.take_cmb.currentIndexChanged.connect(self.update_local)
        self._ui.publish_cmb.currentIndexChanged.connect(self.update_publish)
        self._ui.alts_cmb.currentIndexChanged.connect(self.update_alt)
        self._ui.alts_chk.toggled.connect(Signalling.get().show_alts)
        self._ui.handles_chk.toggled.connect(Signalling.get().show_handles)
        self._ui.name_chk.toggled.connect(Signalling.get().show_name)

        Signalling.get().update_attribute_editor.connect(self.update)
        Signalling.get().change_alt.connect(self.change_alt)
        Signalling.get().change_take.connect(self.change_take)

    @property
    def shot(self):
        return self._ui.shot_edt.text()

    @shot.setter
    def shot(self, value):
        self._ui.shot_edt.setText(value)

    @property
    def local_takes(self):
        return [self._ui.take_cmb.itemData(x) for x in range(self._ui.take_cmb.count())]

    @local_takes.setter
    def local_takes(self, value):
        self._ui.take_cmb.clear()
        # self._ui.take_cmb.addItem("--", {})
        value.sort(key=operator.itemgetter("timestamp"), reverse=True)
        for take in value:
            time_string = ""
            if take.get("timestamp"):
                time_string = time.strftime(
                    "%d/%m/%y %H:%M", time.localtime(take["timestamp"])
                )
            self._ui.take_cmb.addItem(f"{take['name']} {time_string}", take)

        if self._ui.take_cmb.count() == 0:
            self._ui.take_cmb.addItem("--", {})

    @property
    def published_takes(self):
        return [
            self._ui.publish_cmb.itemData(x)
            for x in range(self._ui.publish_cmb.count())
        ]

    @published_takes.setter
    def published_takes(self, value):
        self._ui.publish_cmb.clear()
        self._ui.publish_cmb.addItem("--", {})
        value.sort(key=operator.itemgetter("version"), reverse=True)
        for take in value:
            title = f"{take['name']} " f"v{take['version']:03}"
            self._ui.publish_cmb.addItem(title, take)

    @property
    def current_version(self):
        if self._ui.take_cmb.currentIndex() != 0:
            return self._ui.take_cmb.currentData()
        elif self._ui.publish_cmb.currentIndex() != 0:
            return self._ui.publish_cmb.currentData()

    @current_version.setter
    def current_version(self, value):
        for ii, take in enumerate(self.local_takes):
            if Path(rvc.undoPathSwapVars(take.get("path", ""))) == Path(value):
                self._ui.take_cmb.setCurrentIndex(ii)
                self._ui.publish_cmb.setCurrentIndex(0)
                return
        else:
            self._ui.take_cmb.setCurrentIndex(0)
        for ii, take in enumerate(self.published_takes):
            if Path(rvc.undoPathSwapVars(take.get("path", ""))) == Path(value):
                self._ui.publish_cmb.setCurrentIndex(ii)
                return
        else:
            self._ui.publish_cmb.setCurrentIndex(0)

    @property
    def alts(self):
        alts = {}

        if not self._ui.alts_cmb.count():
            return

        for ii in range(self._ui.alts_cmb.count()):
            alts[self._ui.alts_cmb.itemData(ii)] = self._ui.alts_cmb.itemData(ii)

        return alts

    @alts.setter
    def alts(self, value):
        self._ui.alts_cmb.clear()

        if not value:
            return
        value = {**value, **{self.shot: self._uid}}
        value = {k: v for k, v in sorted(value.items(), key=lambda item: item[1])}

        for ii, (alt, uid) in enumerate(value.items()):
            self._ui.alts_cmb.addItem(alt, uid)

            if alt == self.shot:
                self._ui.alts_cmb.setCurrentIndex(ii)

    @property
    def active_alt(self):
        return self._ui.active_chk.isChecked()

    @active_alt.setter
    def active_alt(self, value):
        return self._ui.active_chk.setChecked(value)

    @property
    def step(self):
        return self._ui.department_cmb.currentText()

    @step.setter
    def step(self, value):
        self._ui.department_cmb.setCurrentIndex(self._ui.department_cmb.findText(value))

    def get_alt_name(self, alt):
        if isinstance(alt, dict):
            return alt.get("name")

        return alt

    def update(self, current_index):
        shot_blocked_signals = self._ui.shot_edt.blockSignals(True)
        take_blocked_signals = self._ui.take_cmb.blockSignals(True)
        publish_blocked_signals = self._ui.publish_cmb.blockSignals(True)
        alt_blocked_signals = self._ui.alts_cmb.blockSignals(True)
        dept_blocked_signals = self._ui.department_cmb.blockSignals(True)
        active_blocked_signals = self._ui.active_chk.blockSignals(True)

        if current_index and current_index.data():
            self._uid = current_index.data(ItemDataRole.ID_ROLE)
            self.shot = current_index.data(QtCore.Qt.DisplayRole)
            self.alts = current_index.data(ItemDataRole.ALTS_ROLE)
            self.local_takes = current_index.data(ItemDataRole.LOCAL_TAKES_ROLE)
            self.published_takes = current_index.data(ItemDataRole.PUBLISHED_TAKES_ROLE)
            self.current_version = current_index.data(ItemDataRole.PATH_ROLE)
            self.step = current_index.data(ItemDataRole.STEP_ROLE)
            self.active_alt = current_index.data(ItemDataRole.ACTIVE_ROLE)
        else:
            self._uid = None
            self._ui.alts_cmb.clear()
            self._ui.take_cmb.clear()
            self._ui.publish_cmb.clear()
            self._ui.shot_edt.clear()
            self.active_alt = False

        self._ui.shot_edt.blockSignals(shot_blocked_signals)
        self._ui.take_cmb.blockSignals(take_blocked_signals)
        self._ui.publish_cmb.blockSignals(publish_blocked_signals)
        self._ui.alts_cmb.blockSignals(alt_blocked_signals)
        self._ui.active_chk.blockSignals(dept_blocked_signals)
        self._ui.active_chk.blockSignals(active_blocked_signals)

    def update_shot(self):
        Signalling.get().cut_list_update_current.emit(self.shot, QtCore.Qt.DisplayRole)

    def update_local(self):
        blocked_signals = self._ui.publish_cmb.blockSignals(True)
        self._ui.publish_cmb.setCurrentIndex(0)
        self._ui.publish_cmb.blockSignals(blocked_signals)
        self.update_take()

    def update_publish(self):
        blocked_signals = self._ui.take_cmb.blockSignals(True)
        self._ui.take_cmb.setCurrentIndex(0)
        self._ui.take_cmb.blockSignals(blocked_signals)
        self.update_take()

    def update_take(self):
        current_version = self.current_version
        if not current_version:
            return

        Signalling.get().cut_list_update_current.emit(
            current_version, ItemDataRole.CURRENT_VERSION_ROLE
        )

    def update_alt(self):
        current_alt_id = self._ui.alts_cmb.currentData()
        Signalling.get().alt_attribute_changed.emit(current_alt_id)

    def change_take(self, direction):
        if self._ui.take_cmb.currentText() != "--":
            self.change_combo(self._ui.take_cmb, direction)
        elif self._ui.publish_cmb.currentText() != "--":
            self.change_combo(self._ui.publish_cmb, direction)

    def change_alt(self, direction):
        self.change_combo(self._ui.alts_cmb, direction)

    def change_combo(self, combo, direction):
        if combo.count() < 1:
            return
        new_index = combo.currentIndex() + direction
        if new_index < 0:
            new_index = combo.count() - 1
        if new_index == combo.count():
            new_index = 0
        combo.setCurrentIndex(new_index)
