import math

from .signalling import Signalling
from .rv_playlist import RvPlaylist

from edit_tools.rv_cut_data import RvCutData
from sequence_tools.sequence_objects import PROXY_EXTENSIONS


def calculate_cut_audio(cut):
    if cut.audio:
        for take in cut.takes:
            if not (take.audio or take.has_audio):
                take.audio = cut.audio.path
                take.audio_offset = cut.audio.take_in
    for shot in cut.shots:
        calculate_audio_offset(shot)


def calculate_audio_offset(shot):
    if shot.take and shot.take.audio:
        sequence_media_start = shot.sequence_in - (shot.shot_in - shot.take.take_in)
        shot.audio_offset = (
            shot.take.audio_offset - sequence_media_start - shot.audio_offset
        )


def sort_order(cut, start):
    new_order = start
    for shot in cut.shots:
        new_order = shot.order
        if start:
            new_order = float(new_order) * 0.00001 + start
        shot.order = new_order

    return new_order


def remove_overlap_from_shots(cut):
    shots = sorted(cut.shots, key=lambda x: x.order or math.inf)

    for ii, shot in enumerate(shots, start=1):
        if shot.order is None:
            continue

        if ii > len(shots) - 1:
            continue

        next_shot = shots[ii]
        if next_shot.order is None:
            continue

        if shot.sequence_out < next_shot.sequence_in:
            continue

        overlap = shot.sequence_out + 1 - next_shot.sequence_in
        if shot.track > next_shot.track:
            if shot.shot_out - overlap > shot.shot_in:
                shot.shot_out -= overlap
                shot.sequence_out -= overlap

        elif next_shot.shot_in + overlap < next_shot.shot_out:
            next_shot.shot_in += overlap
            next_shot.sequence_in += overlap


class DataLoader:
    def __init__(self):
        # self.cut_data = RvCutData(engine=RvPlaylist.app)

        Signalling.get().load_cut.connect(self.do_load)

    def sort_and_add_new_data(self, cut_data, order):
        if cut_data:
            order = sort_order(cut_data, order)
            # self.cut_data += cut_data

        return order, cut_data

    def from_files(self, files, order):
        Signalling.get().start_loading.emit(100, 10)

        cut_data = RvCutData(engine=RvPlaylist.app)
        for file in files:
            # cut_data = None
            if file.endswith(".json"):
                cut_data += RvCutData.from_json(file, engine=RvPlaylist.app)
            else:
                for ext in PROXY_EXTENSIONS:
                    if file.endswith(ext):
                        cut_data += RvCutData.from_mov(file, bundle=RvPlaylist.app)
                        break

            order, cut_data = self.sort_and_add_new_data(cut_data, order)

        return cut_data

    def from_items(self, items, order):
        cut_data = RvCutData.from_cut_items(items, RvPlaylist.app)
        self.sort_and_add_new_data(cut_data, order)
        return cut_data

    def from_versions(self, items, order):
        cut_data = RvCutData.from_version_items(items, RvPlaylist.app)
        self.sort_and_add_new_data(cut_data, order)
        return cut_data

    def return_cut(self, order, cut_data):
        Signalling.get().start_loading.emit(
            len(cut_data.shots) + len(cut_data.takes) + 8, 6
        )
        remove_overlap_from_shots(cut_data)
        calculate_cut_audio(cut_data)

        cut_data.populate_take_versions()

        Signalling.get().update_progress.emit(2)
        # Signalling.get().clear_items.emit()
        Signalling.get().new_cut_items.emit(cut_data)

        # self.cut_data = RvCutData(engine=RvPlaylist.app)

    def get_current_data(self, list_items):
        if list_items:
            from .cut_list.cut_model import ItemDataRole

            self.cut_data = RvCutData.from_rv(list_items, ItemDataRole, RvPlaylist.app)

        return self.cut_data

    def do_load(self, data, list_items, order=None):
        # self.cut_data = self.get_current_data(list_items)

        if isinstance(data, str):
            data = [data]

        files = [x for x in data if isinstance(x, str)]
        items = [x for x in data if isinstance(x, dict) and x.get("type") == "CutItem"]
        versions = [
            x for x in data if isinstance(x, dict) and x.get("type") == "Version"
        ]

        cut_data = None
        if files:
            cut_data = self.from_files(files, order)

        if items:
            cut_data = self.from_items(items, order)

        if versions:
            cut_data = self.from_versions(versions, order)

        if cut_data:
            from .cut_list.cut_model import ItemDataRole

            cur_shot_names = [x.data(ItemDataRole.DISPLAY_ROLE) for x in list_items]

            cut_data.shots.update_shot_names(cur_shot_names)
            cut_data.populate_shots_from_takes()

            self.return_cut(order, cut_data)
