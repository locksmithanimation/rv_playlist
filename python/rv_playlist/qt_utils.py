import sgtk

qt_utils = sgtk.platform.import_framework("ls-framework-qtutils", "qt_utils")
SingletonMixin = qt_utils.SingletonMixin
FocusSpinBox = qt_utils.FocusSpinBox
DragSpinBox = qt_utils.DragSpinBox


class DragFocusSpinBox(DragSpinBox, FocusSpinBox):
    pass