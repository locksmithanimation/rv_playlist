from sgtk.platform.qt import QtGui, QtCore

from .ui.publish_form import Ui_Publish_Form


class RvPublishForm(QtGui.QDialog):
    def __init__(self, settings):
        super(RvPublishForm, self).__init__()

        self.ui = Ui_Publish_Form()
        self.ui.setupUi(self)

        self.cancelled = True
        self.settings = settings
        self.ui.description_text.setStyleSheet("color: #FFFFFF;")

        self.ui.cut_publish_radio.setChecked(False)
        self.ui.playlist_publish_radio.setChecked(False)

        if self.settings.contains(self.ui.cut_publish_radio.text()):
            self.ui.cut_publish_radio.setChecked(
                str(self.settings.value(self.ui.cut_publish_radio.objectName()))
                == "true"
            )

        if self.settings.contains(self.ui.playlist_publish_radio.text()):
            self.ui.playlist_publish_radio.setChecked(
                str(self.settings.value(self.ui.playlist_publish_radio.objectName()))
                == "true"
            )

        self.ui.publish_button.clicked.connect(self.publish)
        self.ui.cancel_button.clicked.connect(self.cancel)

    @property
    def description(self):
        return self.ui.description_text.toPlainText()

    @property
    def publish_playlist(self):
        return self.ui.playlist_publish_radio.isChecked()

    @property
    def publish_cut(self):
        return self.ui.cut_publish_radio.isChecked()

    def publish(self):
        self.cancelled = False
        self.set_settings()
        self.close()

    def cancel(self):
        self.cancelled = True
        self.set_settings()
        self.close()

    def set_settings(self):
        self.settings.setValue(
            self.ui.cut_publish_radio.objectName(),
            self.ui.cut_publish_radio.isChecked(),
        )
        self.settings.setValue(
            self.ui.playlist_publish_radio.objectName(),
            self.ui.playlist_publish_radio.isChecked(),
        )
