from PySide2 import QtWidgets

from wip_playblasts import playblast_session

from .ui.playlist_form import Ui_PlaylistForm
from .rv_playlist import RvPlaylist
from .signalling import Signalling


class PlaylistForm(QtWidgets.QFrame):
    def __init__(self, parent=None):
        super(PlaylistForm, self).__init__(parent)
        self._ui = Ui_PlaylistForm()
        self._ui.setupUi(self)

        self._ui.progress_bar.setVisible(False)

        Signalling.get().start_loading.connect(self.setup_progress_bar)
        Signalling.get().update_progress.connect(self.update_progress_bar)
        Signalling.get().load_finished.connect(self.finish_progress_bar)

    def load_data(self):
        session_root = playblast_session.get_playblast_root(RvPlaylist.app)
        file_types = "Accepted File Types (*.mov *.png *.jpg *.json)"
        file_name = QtWidgets.QFileDialog.getOpenFileName(
            self,
            "Open Cut",
            session_root,
            file_types,
        )

        if file_name:
            Signalling.get().load_files.emit(file_name)

    def open_playlist(self):
        session_root = playblast_session.get_playblast_root(RvPlaylist.app)
        file_name = QtWidgets.QFileDialog.getOpenFileName(
            self, "Open Cut", session_root, "Json File (*.json)"
        )
        if file_name[0]:
            Signalling.get().load_files.emit([file_name[0]])

    def setup_progress_bar(self, total, initial_value=0):
        self._ui.progress_bar.setVisible(True)
        self._ui.progress_bar.setMaximum(total)
        self._ui.progress_bar.setValue(initial_value)

    def update_progress_bar(self, amount):
        self._ui.progress_bar.setValue(self._ui.progress_bar.value() + amount)

    def finish_progress_bar(self):
        self._ui.progress_bar.setVisible(False)

    def closeEvent(self, event):
        RvPlaylist.app.logger.debug("RV Playlist Form shut down")
        Signalling.get().closing.emit()
        super().closeEvent(event)
