# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'playlist_form.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from locksmith.Qt.Qt.QtCore import *
from locksmith.Qt.Qt.QtGui import *
from locksmith.Qt.Qt.QtWidgets import *

from ..cut_list.cut_list import CutList
from ..attribute_widget import AttributeWidget

from  . import resources_rc

class Ui_PlaylistForm(object):
    def setupUi(self, PlaylistForm):
        if not PlaylistForm.objectName():
            PlaylistForm.setObjectName(u"PlaylistForm")
        PlaylistForm.resize(589, 733)
        self.add_action = QAction(PlaylistForm)
        self.add_action.setObjectName(u"add_action")
        icon = QIcon()
        icon.addFile(u":/playlist_icons/plus.png", QSize(), QIcon.Normal, QIcon.Off)
        self.add_action.setIcon(icon)
        self.remove_action = QAction(PlaylistForm)
        self.remove_action.setObjectName(u"remove_action")
        icon1 = QIcon()
        icon1.addFile(u":/playlist_icons/minus.png", QSize(), QIcon.Normal, QIcon.Off)
        self.remove_action.setIcon(icon1)
        self.clear_action = QAction(PlaylistForm)
        self.clear_action.setObjectName(u"clear_action")
        icon2 = QIcon()
        icon2.addFile(u":/playlist_icons/clear.png", QSize(), QIcon.Normal, QIcon.Off)
        self.clear_action.setIcon(icon2)
        self.refresh_action = QAction(PlaylistForm)
        self.refresh_action.setObjectName(u"refresh_action")
        icon3 = QIcon()
        icon3.addFile(u":/playlist_icons/refresh.png", QSize(), QIcon.Normal, QIcon.Off)
        self.refresh_action.setIcon(icon3)
        self.open_action = QAction(PlaylistForm)
        self.open_action.setObjectName(u"open_action")
        icon4 = QIcon()
        icon4.addFile(u":/playlist_icons/open.png", QSize(), QIcon.Normal, QIcon.Off)
        self.open_action.setIcon(icon4)
        self.save_action = QAction(PlaylistForm)
        self.save_action.setObjectName(u"save_action")
        self.save_action.setEnabled(True)
        icon5 = QIcon()
        icon5.addFile(u":/playlist_icons/save.png", QSize(), QIcon.Normal, QIcon.Off)
        icon5.addFile(u":/playlist_icons/save_disabled.png", QSize(), QIcon.Disabled, QIcon.Off)
        self.save_action.setIcon(icon5)
        self.publish_action = QAction(PlaylistForm)
        self.publish_action.setObjectName(u"publish_action")
        self.publish_action.setEnabled(True)
        icon6 = QIcon()
        icon6.addFile(u":/playlist_icons/publish.png", QSize(), QIcon.Normal, QIcon.Off)
        icon6.addFile(u":/playlist_icons/publish_disabled.png", QSize(), QIcon.Disabled, QIcon.Off)
        self.publish_action.setIcon(icon6)
        self.verticalLayout_4 = QVBoxLayout(PlaylistForm)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.splitter = QSplitter(PlaylistForm)
        self.splitter.setObjectName(u"splitter")
        self.splitter.setOrientation(Qt.Horizontal)
        self.horizontalLayoutWidget = QWidget(self.splitter)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayout_3 = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setSpacing(4)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(4, -1, 4, -1)
        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer)

        self.toolBar = QToolBar(self.horizontalLayoutWidget)
        self.toolBar.setObjectName(u"toolBar")
        self.toolBar.setOrientation(Qt.Vertical)
        self.toolBar.setIconSize(QSize(20, 20))

        self.verticalLayout_3.addWidget(self.toolBar)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer_2)


        self.horizontalLayout_3.addLayout(self.verticalLayout_3)

        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(-1, -1, -1, 6)
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalSpacer = QSpacerItem(41, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.label = QLabel(self.horizontalLayoutWidget)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_2.addWidget(self.label)

        self.label_3 = QLabel(self.horizontalLayoutWidget)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setMinimumSize(QSize(81, 0))
        self.label_3.setMaximumSize(QSize(81, 18))
        self.label_3.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_2.addWidget(self.label_3)


        self.verticalLayout_2.addLayout(self.horizontalLayout_2)

        self.progress_bar = QProgressBar(self.horizontalLayoutWidget)
        self.progress_bar.setObjectName(u"progress_bar")
        self.progress_bar.setMaximumSize(QSize(16777215, 10))
        self.progress_bar.setValue(0)
        self.progress_bar.setTextVisible(False)

        self.verticalLayout_2.addWidget(self.progress_bar)

        self.cut_lst = CutList(self.horizontalLayoutWidget)
        self.cut_lst.setObjectName(u"cut_lst")
        self.cut_lst.setMouseTracking(True)
        self.cut_lst.setContextMenuPolicy(Qt.CustomContextMenu)
        self.cut_lst.setAcceptDrops(True)
        self.cut_lst.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.cut_lst.setProperty("showDropIndicator", False)
        self.cut_lst.setDragEnabled(True)
        self.cut_lst.setDragDropOverwriteMode(False)
        self.cut_lst.setDragDropMode(QAbstractItemView.InternalMove)
        self.cut_lst.setDefaultDropAction(Qt.MoveAction)
        self.cut_lst.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.verticalLayout_2.addWidget(self.cut_lst)


        self.horizontalLayout_3.addLayout(self.verticalLayout_2)

        self.splitter.addWidget(self.horizontalLayoutWidget)
        self.verticalLayoutWidget = QWidget(self.splitter)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 6, 0)
        self.attribute_wgt = AttributeWidget(self.verticalLayoutWidget)
        self.attribute_wgt.setObjectName(u"attribute_wgt")

        self.verticalLayout.addWidget(self.attribute_wgt)

        self.splitter.addWidget(self.verticalLayoutWidget)

        self.verticalLayout_4.addWidget(self.splitter)


        self.toolBar.addAction(self.add_action)
        self.toolBar.addAction(self.remove_action)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.clear_action)
        self.toolBar.addAction(self.refresh_action)
        self.toolBar.addAction(self.open_action)
        self.toolBar.addAction(self.save_action)
        self.toolBar.addAction(self.publish_action)

        self.retranslateUi(PlaylistForm)
        self.add_action.triggered.connect(PlaylistForm.load_data)
        self.remove_action.triggered.connect(self.cut_lst.remove_selected)
        self.open_action.triggered.connect(PlaylistForm.open_playlist)
        self.clear_action.triggered.connect(self.cut_lst.clear_dialog)
        self.refresh_action.triggered.connect(self.cut_lst.refresh)
        self.save_action.triggered.connect(self.cut_lst.save)
        self.publish_action.triggered.connect(self.cut_lst.publish)

        QMetaObject.connectSlotsByName(PlaylistForm)
    # setupUi

    def retranslateUi(self, PlaylistForm):
        PlaylistForm.setWindowTitle(QCoreApplication.translate("PlaylistForm", u"RV Playlist", None))
        self.add_action.setText(QCoreApplication.translate("PlaylistForm", u"add", None))
#if QT_CONFIG(tooltip)
        self.add_action.setToolTip(QCoreApplication.translate("PlaylistForm", u"Add media", None))
#endif // QT_CONFIG(tooltip)
        self.remove_action.setText(QCoreApplication.translate("PlaylistForm", u"remove", None))
#if QT_CONFIG(tooltip)
        self.remove_action.setToolTip(QCoreApplication.translate("PlaylistForm", u"Remove media", None))
#endif // QT_CONFIG(tooltip)
        self.clear_action.setText(QCoreApplication.translate("PlaylistForm", u"clear", None))
#if QT_CONFIG(tooltip)
        self.clear_action.setToolTip(QCoreApplication.translate("PlaylistForm", u"Clear playlist", None))
#endif // QT_CONFIG(tooltip)
        self.refresh_action.setText(QCoreApplication.translate("PlaylistForm", u"refresh", None))
#if QT_CONFIG(tooltip)
        self.refresh_action.setToolTip(QCoreApplication.translate("PlaylistForm", u"Refresh playlist", None))
#endif // QT_CONFIG(tooltip)
        self.open_action.setText(QCoreApplication.translate("PlaylistForm", u"open", None))
#if QT_CONFIG(tooltip)
        self.open_action.setToolTip(QCoreApplication.translate("PlaylistForm", u"Open playlist", None))
#endif // QT_CONFIG(tooltip)
        self.save_action.setText(QCoreApplication.translate("PlaylistForm", u"save", None))
#if QT_CONFIG(tooltip)
        self.save_action.setToolTip(QCoreApplication.translate("PlaylistForm", u"Save playlist", None))
#endif // QT_CONFIG(tooltip)
        self.publish_action.setText(QCoreApplication.translate("PlaylistForm", u"publish", None))
#if QT_CONFIG(tooltip)
        self.publish_action.setToolTip(QCoreApplication.translate("PlaylistForm", u"Publish playlist", None))
#endif // QT_CONFIG(tooltip)
        self.toolBar.setWindowTitle(QCoreApplication.translate("PlaylistForm", u"toolBar", None))
        self.label.setText(QCoreApplication.translate("PlaylistForm", u"Shot", None))
        self.label_3.setText(QCoreApplication.translate("PlaylistForm", u"Range", None))
    # retranslateUi

