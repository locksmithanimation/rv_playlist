# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'shot_widget.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from locksmith.Qt.Qt.QtCore import *
from locksmith.Qt.Qt.QtGui import *
from locksmith.Qt.Qt.QtWidgets import *

from ..qt_utils import DragFocusSpinBox

from  . import resources_rc

class Ui_ShotWidget(object):
    def setupUi(self, ShotWidget):
        if not ShotWidget.objectName():
            ShotWidget.setObjectName(u"ShotWidget")
        ShotWidget.resize(325, 34)
        ShotWidget.setAcceptDrops(True)
        self.horizontalLayout = QHBoxLayout(ShotWidget)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(1, 1, 1, 1)
        self.background = QFrame(ShotWidget)
        self.background.setObjectName(u"background")
        self.background.setFrameShape(QFrame.StyledPanel)
        self.background.setFrameShadow(QFrame.Raised)
        self.background.setProperty("state", 0)
        self.verticalLayout = QVBoxLayout(self.background)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setSpacing(2)
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.horizontalLayout_6.setContentsMargins(5, 5, 5, 5)
        self.enabled_chk = QCheckBox(self.background)
        self.enabled_chk.setObjectName(u"enabled_chk")
        self.enabled_chk.setMaximumSize(QSize(20, 16777215))
        self.enabled_chk.setChecked(True)

        self.horizontalLayout_6.addWidget(self.enabled_chk)

        self.order_lbl = QLabel(self.background)
        self.order_lbl.setObjectName(u"order_lbl")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.order_lbl.sizePolicy().hasHeightForWidth())
        self.order_lbl.setSizePolicy(sizePolicy)
        self.order_lbl.setMinimumSize(QSize(20, 0))
        self.order_lbl.setMaximumSize(QSize(20, 16777215))
        self.order_lbl.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_6.addWidget(self.order_lbl)

        self.name_lbl = QLabel(self.background)
        self.name_lbl.setObjectName(u"name_lbl")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.name_lbl.sizePolicy().hasHeightForWidth())
        self.name_lbl.setSizePolicy(sizePolicy1)
        self.name_lbl.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_6.addWidget(self.name_lbl)

        self.shotin_box = DragFocusSpinBox(self.background)
        self.shotin_box.setObjectName(u"shotin_box")
        sizePolicy2 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.shotin_box.sizePolicy().hasHeightForWidth())
        self.shotin_box.setSizePolicy(sizePolicy2)
        self.shotin_box.setMinimumSize(QSize(34, 18))
        self.shotin_box.setMaximumSize(QSize(34, 18))
        font = QFont()
        font.setPointSize(7)
        self.shotin_box.setFont(font)
        self.shotin_box.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.shotin_box.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.shotin_box.setMinimum(0)
        self.shotin_box.setMaximum(999999)
        self.shotin_box.setProperty("value_changed", False)

        self.horizontalLayout_6.addWidget(self.shotin_box)

        self.shotout_box = DragFocusSpinBox(self.background)
        self.shotout_box.setObjectName(u"shotout_box")
        sizePolicy2.setHeightForWidth(self.shotout_box.sizePolicy().hasHeightForWidth())
        self.shotout_box.setSizePolicy(sizePolicy2)
        self.shotout_box.setMinimumSize(QSize(34, 18))
        self.shotout_box.setMaximumSize(QSize(34, 18))
        self.shotout_box.setFont(font)
        self.shotout_box.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.shotout_box.setButtonSymbols(QAbstractSpinBox.NoButtons)
        self.shotout_box.setMinimum(0)
        self.shotout_box.setMaximum(999999)
        self.shotout_box.setProperty("value_changed", False)

        self.horizontalLayout_6.addWidget(self.shotout_box)


        self.verticalLayout.addLayout(self.horizontalLayout_6)


        self.horizontalLayout.addWidget(self.background)

        QWidget.setTabOrder(self.enabled_chk, self.shotin_box)
        QWidget.setTabOrder(self.shotin_box, self.shotout_box)

        self.retranslateUi(ShotWidget)

        QMetaObject.connectSlotsByName(ShotWidget)
    # setupUi

    def retranslateUi(self, ShotWidget):
        ShotWidget.setWindowTitle(QCoreApplication.translate("ShotWidget", u"Form", None))
        self.enabled_chk.setText("")
        self.order_lbl.setText("")
        self.name_lbl.setText(QCoreApplication.translate("ShotWidget", u"Shot Name", None))
    # retranslateUi

