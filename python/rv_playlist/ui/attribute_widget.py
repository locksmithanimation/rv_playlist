# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'attribute_widget.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from locksmith.Qt.Qt.QtCore import *
from locksmith.Qt.Qt.QtGui import *
from locksmith.Qt.Qt.QtWidgets import *


class Ui_AttributeWidget(object):
    def setupUi(self, AttributeWidget):
        if not AttributeWidget.objectName():
            AttributeWidget.setObjectName(u"AttributeWidget")
        AttributeWidget.resize(377, 737)
        self.verticalLayout = QVBoxLayout(AttributeWidget)
        self.verticalLayout.setSpacing(10)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(20, 35, 6, 0)
        self.groupBox = QGroupBox(AttributeWidget)
        self.groupBox.setObjectName(u"groupBox")
        self.gridLayout_2 = QGridLayout(self.groupBox)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(-1, 30, -1, -1)
        self.alts_cmb = QComboBox(self.groupBox)
        self.alts_cmb.setObjectName(u"alts_cmb")

        self.gridLayout_2.addWidget(self.alts_cmb, 3, 1, 1, 1)

        self.label_5 = QLabel(self.groupBox)
        self.label_5.setObjectName(u"label_5")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)
        self.label_5.setMinimumSize(QSize(80, 0))
        self.label_5.setMaximumSize(QSize(16777215, 18))
        self.label_5.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_5, 0, 0, 1, 1)

        self.take_cmb = QComboBox(self.groupBox)
        self.take_cmb.setObjectName(u"take_cmb")

        self.gridLayout_2.addWidget(self.take_cmb, 1, 1, 1, 1)

        self.publish_cmb = QComboBox(self.groupBox)
        self.publish_cmb.setObjectName(u"publish_cmb")

        self.gridLayout_2.addWidget(self.publish_cmb, 2, 1, 1, 1)

        self.active_chk = QCheckBox(self.groupBox)
        self.active_chk.setObjectName(u"active_chk")

        self.gridLayout_2.addWidget(self.active_chk, 5, 1, 1, 1)

        self.label_8 = QLabel(self.groupBox)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setMinimumSize(QSize(80, 0))
        self.label_8.setMaximumSize(QSize(16777215, 18))
        self.label_8.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_8, 2, 0, 1, 1)

        self.label_4 = QLabel(self.groupBox)
        self.label_4.setObjectName(u"label_4")
        sizePolicy.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy)
        self.label_4.setMinimumSize(QSize(80, 0))
        self.label_4.setMaximumSize(QSize(16777215, 18))
        self.label_4.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_4, 1, 0, 1, 1)

        self.label = QLabel(self.groupBox)
        self.label.setObjectName(u"label")
        self.label.setMinimumSize(QSize(80, 0))
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label, 3, 0, 1, 1)

        self.label_7 = QLabel(self.groupBox)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setMinimumSize(QSize(40, 0))

        self.gridLayout_2.addWidget(self.label_7, 5, 0, 1, 1)

        self.shot_edt = QLineEdit(self.groupBox)
        self.shot_edt.setObjectName(u"shot_edt")

        self.gridLayout_2.addWidget(self.shot_edt, 0, 1, 1, 1)

        self.department_cmb = QComboBox(self.groupBox)
        self.department_cmb.addItem("")
        self.department_cmb.addItem("")
        self.department_cmb.addItem("")
        self.department_cmb.addItem("")
        self.department_cmb.setObjectName(u"department_cmb")

        self.gridLayout_2.addWidget(self.department_cmb, 4, 1, 1, 1)

        self.label_9 = QLabel(self.groupBox)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setMinimumSize(QSize(80, 0))
        self.label_9.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_9, 4, 0, 1, 1)

        self.gridLayout_2.setColumnStretch(0, 1)
        self.gridLayout_2.setColumnStretch(1, 100)

        self.verticalLayout.addWidget(self.groupBox)

        self.groupBox_2 = QGroupBox(AttributeWidget)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.gridLayout = QGridLayout(self.groupBox_2)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(-1, 30, -1, -1)
        self.label_3 = QLabel(self.groupBox_2)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setMinimumSize(QSize(40, 0))

        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)

        self.alts_chk = QCheckBox(self.groupBox_2)
        self.alts_chk.setObjectName(u"alts_chk")

        self.gridLayout.addWidget(self.alts_chk, 1, 1, 1, 1)

        self.label_2 = QLabel(self.groupBox_2)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setMinimumSize(QSize(80, 0))

        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)

        self.handles_chk = QCheckBox(self.groupBox_2)
        self.handles_chk.setObjectName(u"handles_chk")

        self.gridLayout.addWidget(self.handles_chk, 0, 1, 1, 1)

        self.name_chk = QCheckBox(self.groupBox_2)
        self.name_chk.setObjectName(u"name_chk")
        self.name_chk.setChecked(True)

        self.gridLayout.addWidget(self.name_chk, 2, 1, 1, 1)

        self.label_6 = QLabel(self.groupBox_2)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setMinimumSize(QSize(40, 0))

        self.gridLayout.addWidget(self.label_6, 2, 0, 1, 1)

        self.gridLayout.setColumnStretch(0, 1)
        self.gridLayout.setColumnStretch(1, 100)

        self.verticalLayout.addWidget(self.groupBox_2)

        self.verticalSpacer_4 = QSpacerItem(115, 644, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_4)


        self.retranslateUi(AttributeWidget)

        QMetaObject.connectSlotsByName(AttributeWidget)
    # setupUi

    def retranslateUi(self, AttributeWidget):
        AttributeWidget.setWindowTitle(QCoreApplication.translate("AttributeWidget", u"Form", None))
        self.groupBox.setTitle(QCoreApplication.translate("AttributeWidget", u"Attributes", None))
        self.label_5.setText(QCoreApplication.translate("AttributeWidget", u"Shot", None))
        self.active_chk.setText(QCoreApplication.translate("AttributeWidget", u"Active Alt", None))
        self.label_8.setText(QCoreApplication.translate("AttributeWidget", u"Published Take", None))
        self.label_4.setText(QCoreApplication.translate("AttributeWidget", u"Local Take", None))
        self.label.setText(QCoreApplication.translate("AttributeWidget", u"Alts", None))
        self.label_7.setText("")
        self.department_cmb.setItemText(0, QCoreApplication.translate("AttributeWidget", u"Animation", None))
        self.department_cmb.setItemText(1, QCoreApplication.translate("AttributeWidget", u"Editorial", None))
        self.department_cmb.setItemText(2, QCoreApplication.translate("AttributeWidget", u"Layout", None))
        self.department_cmb.setItemText(3, QCoreApplication.translate("AttributeWidget", u"Tech Layout", None))

        self.label_9.setText(QCoreApplication.translate("AttributeWidget", u"Department", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("AttributeWidget", u"Settings", None))
        self.label_3.setText("")
        self.alts_chk.setText(QCoreApplication.translate("AttributeWidget", u"Show Alts", None))
        self.label_2.setText("")
        self.handles_chk.setText(QCoreApplication.translate("AttributeWidget", u"Show Handles", None))
        self.name_chk.setText(QCoreApplication.translate("AttributeWidget", u"Show Shot Name", None))
        self.label_6.setText("")
    # retranslateUi

