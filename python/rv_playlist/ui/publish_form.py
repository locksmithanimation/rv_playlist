# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'publish_form.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from locksmith.Qt.Qt.QtCore import *
from locksmith.Qt.Qt.QtGui import *
from locksmith.Qt.Qt.QtWidgets import *


class Ui_Publish_Form(object):
    def setupUi(self, Publish_Form):
        if not Publish_Form.objectName():
            Publish_Form.setObjectName(u"Publish_Form")
        Publish_Form.resize(310, 240)
        self.verticalLayout = QVBoxLayout(Publish_Form)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.cut_publish_radio = QRadioButton(Publish_Form)
        self.cut_publish_radio.setObjectName(u"cut_publish_radio")

        self.horizontalLayout_2.addWidget(self.cut_publish_radio)

        self.playlist_publish_radio = QRadioButton(Publish_Form)
        self.playlist_publish_radio.setObjectName(u"playlist_publish_radio")

        self.horizontalLayout_2.addWidget(self.playlist_publish_radio)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.description_text = QPlainTextEdit(Publish_Form)
        self.description_text.setObjectName(u"description_text")
        self.description_text.setFrameShape(QFrame.Box)

        self.verticalLayout.addWidget(self.description_text)

        self.verticalSpacer = QSpacerItem(20, 13, QSizePolicy.Minimum, QSizePolicy.Minimum)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.cancel_button = QPushButton(Publish_Form)
        self.cancel_button.setObjectName(u"cancel_button")

        self.horizontalLayout.addWidget(self.cancel_button)

        self.publish_button = QPushButton(Publish_Form)
        self.publish_button.setObjectName(u"publish_button")

        self.horizontalLayout.addWidget(self.publish_button)


        self.verticalLayout.addLayout(self.horizontalLayout)


        self.retranslateUi(Publish_Form)

        QMetaObject.connectSlotsByName(Publish_Form)
    # setupUi

    def retranslateUi(self, Publish_Form):
        Publish_Form.setWindowTitle(QCoreApplication.translate("Publish_Form", u"RV Publish", None))
        self.cut_publish_radio.setText(QCoreApplication.translate("Publish_Form", u"Publish Cut", None))
        self.playlist_publish_radio.setText(QCoreApplication.translate("Publish_Form", u"Publish Playlist", None))
        self.description_text.setPlaceholderText(QCoreApplication.translate("Publish_Form", u"Enter publish description...", None))
        self.cancel_button.setText(QCoreApplication.translate("Publish_Form", u"Cancel", None))
        self.publish_button.setText(QCoreApplication.translate("Publish_Form", u"Publish", None))
    # retranslateUi

