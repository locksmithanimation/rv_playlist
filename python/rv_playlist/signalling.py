import sgtk
from sgtk.platform.qt import QtCore, QtGui

from .qt_utils import SingletonMixin


class Signalling(QtCore.QObject, SingletonMixin):
    closing = QtCore.Signal()
    load_files = QtCore.Signal(list)
    load_cut = QtCore.Signal(list, list, float)
    new_cut_items = QtCore.Signal(dict)
    request_edl = QtCore.Signal()

    # signals to RV
    load_sources = QtCore.Signal(dict)
    delete_sources = QtCore.Signal(list)
    new_edl = QtCore.Signal(dict, bool, bool)
    clear_playlist = QtCore.Signal()
    clear_items = QtCore.Signal()
    qt_shot_changed = QtCore.Signal(str, int, int)
    update_shot = QtCore.Signal(str, int, int)
    set_source_frame = QtCore.Signal(int)

    # signals from RV
    new_source_group = QtCore.Signal(str, list)
    rv_loading_finished = QtCore.Signal()
    rv_shot_changed = QtCore.Signal(int)
    rv_edl_loaded = QtCore.Signal()
    update_shot_from_qt = QtCore.Signal()

    # attribute editor singals
    update_attribute_editor = QtCore.Signal(QtCore.QModelIndex)
    cut_list_update_current = QtCore.Signal(object, int)
    alt_attribute_changed = QtCore.Signal(int)
    show_alts = QtCore.Signal(bool)
    show_handles = QtCore.Signal(bool)
    show_name = QtCore.Signal(bool)

    # progress bar
    start_loading = QtCore.Signal(int, int)
    update_progress = QtCore.Signal(int)
    load_finished = QtCore.Signal()

    # hotkey signals
    change_take = QtCore.Signal(int)
    change_alt = QtCore.Signal(int)

    set_auto_edl = QtCore.Signal(int)

    set_to_stream = QtCore.Signal()
    set_to_movie = QtCore.Signal()

    def __init__(self, parent=None):
        super(Signalling, self).__init__(parent)
        self.set_signalling_obj(self)
