import rv.commands as rvc


def take_version_up(event):
    from .signalling import Signalling

    Signalling.get().change_take.emit(1)


def take_version_down(event):
    from .signalling import Signalling

    Signalling.get().change_take.emit(-1)


def alt_version_up(event):
    from .signalling import Signalling

    Signalling.get().change_alt.emit(1)


def alt_version_down(event):
    from .signalling import Signalling

    Signalling.get().change_alt.emit(-1)


def bind_hotkeys(hotkey_data):
    for hotkey_datum in hotkey_data.values():
        func = globals()[hotkey_datum["command"]]
        rvc.bind(
            "default",
            "global",
            hotkey_datum["key"],
            func,
            "",
        )
