import rv.commands as rvc
import rv.extra_commands as rve
import rv.runtime as rvr
import rv.rvtypes as rvtypes

from . import rv_commands
from .rv_sequence import RvSequence
from ..signalling import Signalling
from ..rv_playlist import RvPlaylist


class RvLiveReview:
    def __init__(self):
        bindings = {
            (
                "live-review-create-session",
                self.created_live_review,
                "",
            ),
            (
                "live-review-join-session",
                self.joined_live_review,
                "",
            ),
            (
                "live-review-leave-session",
                self.leaving_live_review,
                "",
            ),
            (
                "live-review-assign-presenter",
                self.assign_presenter,
                "",
            ),
            (
                "mq-new-presenter",
                self.assign_presenter,
                "",
            ),
        }

        for b in bindings:
            (event, func, docs) = b
            rvc.bind("default", "global", event, func, docs)

    def block_signals(self):
        Signalling.get().blockSignals(True)

    def unblock_signals(self):
        Signalling.get().blockSignals(False)

    def created_live_review(self, event=None):
        # event and event.reject()

        RvPlaylist.app.logger.debug(f"Created live review session")
        Signalling.get().set_to_stream.emit()

    def joined_live_review(self, event=None):
        # event and event.reject()

        RvPlaylist.app.logger.debug(f"Joined live review session")
        self.block_signals()

    def leaving_live_review(self, event=None):
        # event and event.reject()

        RvPlaylist.app.logger.debug(f"Leaving live review session")
        Signalling.get().set_to_movie.emit()
        self.unblock_signals()

    def assign_presenter(self, event=None):
        # event and event.reject()
        RvPlaylist.app.logger.debug(f"Presenter was changed to {event.contents()}")


class RvSession(object):
    def __init__(self):
        rvc.setProgressiveSourceLoading(True)
        self.sequence = RvSequence()
        self.live_review = RvLiveReview()
        self._playing = False

        self.pending_loads = {}
        self.block_frame_event = False
        self.current_index = -1

        self._show_shot_numbers = True
        self._anchor_frame = None

        self.setup_bindings()

        Signalling.get().load_sources.connect(self.load_sources)
        Signalling.get().delete_sources.connect(self.delete_sources)
        Signalling.get().qt_shot_changed.connect(self.change_shot)
        Signalling.get().update_shot.connect(self.update_shot)
        Signalling.get().set_source_frame.connect(self.set_source_frame)
        Signalling.get().show_name.connect(self.show_shot_number)

    def setup_bindings(self):
        rvc.bind(
            "default", "global", "source-group-complete", self.source_group_loaded, ""
        )
        rvc.bind(
            "default", "global", "after-progressive-loading", self.loading_finished, ""
        )
        rvc.bind("default", "global", "frame-changed", self.did_shot_change, "")
        rvc.bind("default", "global", "play-start", self.playback_started, "")
        rvc.bind("default", "global", "play-stop", self.playback_stopped, "")

    def playback_started(self, *args):
        self._playing = True

    def playback_stopped(self, *args):
        self._playing = False

    def show_shot_number(self, value):
        self._show_shot_numbers = value
        Signalling.get().update_shot_from_qt.emit()

    def set_source_frame(self, new_frame):
        global_frame = rvc.frame()
        source_frame = rve.sourceFrame(global_frame)
        rvc.setFrame(global_frame + new_frame - source_frame)

    def change_shot(self, shot_name, audio_offset, edl_index):
        self.update_shot(shot_name, audio_offset, edl_index)
        if self._playing:
            return
        self.block_frame_event = True
        self.sequence.view_sequence()
        try:
            current_index = self.sequence.index_from_edl()
            if current_index < 0 or current_index != edl_index:
                Signalling.get().blockSignals(True)
                rvc.setFrame(
                    rvc.getIntProperty("{}.edl.frame".format(self.sequence.sequence))[
                        edl_index
                    ]
                )
            self.current_index = edl_index
        except IndexError:
            pass
        finally:
            Signalling.get().blockSignals(False)
            self.block_frame_event = False

    def update_shot(self, shot_name, audio_offset, edl_index):
        if not self._show_shot_numbers:
            shot_name = ""
        self.sequence.update_shot(shot_name, audio_offset, edl_index)

    def load_sources(self, data):
        self._anchor_frame = rvc.frame()
        self.block_frame_event = True
        self.sequence.set_edl()
        new_sources = []
        existing_sources = []
        for take in data.values():
            shots_ids = take["uids"]
            path = rvc.undoPathSwapVars(take["path"])
            self.pending_loads[path] = shots_ids
            existing_source = rv_commands.does_source_exist(path)
            if existing_source:
                existing_sources.append(existing_source)
                continue
            new_sources.extend(rv_commands.create_source_string(take))
        if new_sources:
            rvc.addSourceBegin()
            rvc.addSources(new_sources, "", True)
        for existing_source in existing_sources:
            self.source_group_loaded(source_group=existing_source)
        if new_sources or (not new_sources and existing_sources):
            self.loading_finished()
        else:
            self.block_frame_event = False

    def source_group_loaded(self, data=None, source_group=None):
        Signalling.get().update_progress.emit(1)
        if data:
            source_group, _ = data.contents().split(";;")
        if not source_group:
            raise AttributeError("Event data or a source_group must be provided.")
        file_source = rv_commands.get_node_from_group(source_group, "RVFileSource")
        paths = rvc.sourceMedia(file_source)
        for path in paths:
            if not path.endswith(".wav"):
                break
        else:
            return
        if path not in self.pending_loads:
            return
        if path.startswith("https"):
            sg_id = path.split("/")[-2]
            rve.setUIName(source_group, f"Version {sg_id} (Stream)")

        uids = self.pending_loads.pop(path)
        self.sequence.connect_source(source_group)
        Signalling.get().new_source_group.emit(source_group, uids)

    def loading_finished(self, *args):
        if not self.pending_loads:
            rvc.addSourceEnd()
            Signalling.get().rv_loading_finished.emit()
            if self._anchor_frame is not None:
                self.sequence.view_sequence()
                rvc.setFrame(self._anchor_frame)
                self._anchor_frame = None
            self.block_frame_event = False

    def delete_sources(self, source_groups):
        self.block_frame_event = True
        for source_group in source_groups:
            rvc.deleteNode(source_group)
        self.block_frame_event = False

    def did_shot_change(self, event=None, force=False):
        if self.block_frame_event:
            return

        current_index = self.sequence.index_from_edl()
        if force or current_index < 0 or current_index != self.current_index:
            Signalling.get().rv_shot_changed.emit(current_index)

    def shut_down(self):
        rvc.unbind("default", "global", "source-group-complete")
        rvc.unbind("default", "global", "after-progressive-loading")
        rvc.unbind("default", "global", "frame-changed")
        rvc.unbind("default", "global", "play-start")
        rvc.unbind("default", "global", "play-stop")
