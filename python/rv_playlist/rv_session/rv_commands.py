from pathlib import Path
import rv.commands as rvc


def get_node_from_group(group, node_type):
    node = [x for x in rvc.nodesInGroup(group) if rvc.nodeType(x) == node_type]
    return node and node[0] or None


def does_source_exist(path):
    source_group = None
    path = Path(path)
    if rvc.sources() and path in [Path(x[0]) for x in rvc.sources()]:
        for file_source in rvc.nodesOfType("RVFileSource"):
            if Path(rvc.sourceMedia(file_source)[0]) == path:
                source_group = rvc.nodeGroup(file_source)
                break
    return source_group


def create_source_string(take):
    source = []
    path = take["path"]
    if not take["path"].startswith("https"):
        path = Path(take["path"]).as_posix()
    source.append("[")
    source.append(path)
    if take["audio_path"]:
        source.append(Path(take["audio_path"]).as_posix())
        source.append("+ao")
        audio_offset = take["audio_offset"] or 0
        source.append(str(audio_offset / 24.0))
    source.append("+rs")
    source.append(str(take["take_in"]))
    source.append("]")
    return source


def create_text(node, value, h_pos, v_pos):
    rvc.newProperty(f"{node}.position", rvc.FloatType, 2)
    rvc.newProperty(f"{node}.color", rvc.FloatType, 4)
    rvc.newProperty(f"{node}.spacing", rvc.FloatType, 1)
    rvc.newProperty(f"{node}.size", rvc.FloatType, 1)
    rvc.newProperty(f"{node}.scale", rvc.FloatType, 1)
    rvc.newProperty(f"{node}.rotation", rvc.FloatType, 1)
    rvc.newProperty(f"{node}.font", rvc.StringType, 1)
    rvc.newProperty(f"{node}.text", rvc.StringType, 1)
    rvc.newProperty(f"{node}.origin", rvc.StringType, 1)
    rvc.newProperty(f"{node}.debug", rvc.IntType, 1)

    rvc.setFloatProperty(f"{node}.position", [float(h_pos), float(v_pos)], True)
    rvc.setFloatProperty(f"{node}.color", [0.0, 1.0, 0.0, 1.0], True)
    rvc.setFloatProperty(f"{node}.spacing", [1.0], True)
    rvc.setFloatProperty(f"{node}.size", [0.002], True)
    rvc.setFloatProperty(f"{node}.scale", [1.0], True)
    rvc.setFloatProperty(f"{node}.rotation", [0.0], True)
    rvc.setStringProperty(f"{node}.font", [""], True)
    rvc.setStringProperty(f"{node}.text", [value], True)
    rvc.setStringProperty(f"{node}.origin", ["center-center"], True)
    rvc.setIntProperty(f"{node}.debug", [0], True)


def update_text(node, value):
    rvc.setStringProperty(f"{node}.text", [value], True)


def follow_graph(node, direction, distance):
    if distance:
        connection = rvc.nodeConnections(node)[direction][0]
        if distance > 1:
            connection = follow_graph(connection, direction, distance - 1)
    return connection
