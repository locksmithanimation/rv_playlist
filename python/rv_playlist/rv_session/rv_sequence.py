import rv.commands as rvc
import rv.extra_commands as rve

from . import rv_commands
from ..signalling import Signalling

PLAYLIST_NAME = "LS_Playlist"


class RvSequence(object):
    def __init__(self):
        self._sequence_group = None
        self._sequence = None
        self.block_frame_event = False

        Signalling.get().new_edl.connect(self.set_edl)
        Signalling.get().clear_playlist.connect(self.clear)
        Signalling.get().set_auto_edl.connect(self.set_auto_edl)

    @property
    def sequence(self):
        if not self._sequence or self._sequence not in rvc.nodesOfType("RVSequence"):
            self._sequence = rv_commands.get_node_from_group(
                self.sequence_group, "RVSequence"
            )
        return self._sequence

    @property
    def sequence_group(self):
        if not self._sequence_group or self._sequence_group not in rvc.nodesOfType(
            "RVSequenceGroup"
        ):
            if PLAYLIST_NAME not in rvc.nodesOfType("RVSequenceGroup"):
                self._sequence_group = rvc.newNode("RVSequenceGroup", PLAYLIST_NAME)
                rve.setUIName(self._sequence_group, PLAYLIST_NAME.replace("_", " "))
            else:
                self._sequence_group = PLAYLIST_NAME
            # rvc.setViewNode(self._sequence_group)  ## probably move
        return self._sequence_group

    def clear(self):
        rvc.clearSession()
        try:
            rvc.deleteNode(self._sequence_group)
        except:
            pass
        self._sequence_group = None
        self._sequence = None

    def view_sequence(self):
        rvc.setViewNode(self.sequence_group)

    def update_shot(self, shot_name, audio_offset, edl_index):
        source_group = self.source_at_index(edl_index)
        if not source_group:
            return
        source = rv_commands.get_node_from_group(source_group, "RVFileSource")
        self.update_audio(audio_offset, source)
        self.update_overlay(shot_name, source)

    def create_overlay(self, shot_name, source):
        overlay = rv_commands.follow_graph(source, 1, 6)
        rv_commands.create_text(f"{overlay}.text:shot", shot_name, 0, -0.43)

    def update_overlay(self, shot_name, source):
        overlay = rv_commands.follow_graph(source, 1, 6)
        rv_commands.update_text(f"{overlay}.text:shot", shot_name)

    def update_audio(self, audio_offset, source):
        new_offset = audio_offset / 24.0
        current_offset = rvc.getFloatProperty(f"{source}.group.audioOffset")
        if new_offset != current_offset:
            rvc.setFloatProperty(f"{source}.group.audioOffset", [new_offset], True)

    def set_edl(self, edl=None, return_signal=False, preserve_position=False):
        edl = edl or {}
        if edl.get("source"):
            sources = [self.index_for_source(x) for x in edl["source"]]
            if -1 in sources:
                return
            rvc.setIntProperty(f"{self.sequence}.mode.useCutInfo", [0], True)
            rvc.setIntProperty(f"{self.sequence}.mode.autoEDL", [0], True)
            rvc.setIntProperty(f"{self.sequence}.output.autoSize", [1], True)
            rvc.setIntProperty(f"{self.sequence}.edl.frame", edl["frame"], True)
            rvc.setIntProperty(f"{self.sequence}.edl.source", sources + [0], True)
            rvc.setIntProperty(f"{self.sequence}.edl.in", edl["shot_in"] + [0], True)
            rvc.setIntProperty(f"{self.sequence}.edl.out", edl["shot_out"] + [0], True)
            if preserve_position:
                global_frame = rvc.frame()
                source_frame = rve.sourceFrame(global_frame)
                file_source = rvc.sourcesAtFrame(global_frame)
                if file_source:
                    source_group = rvc.nodeGroup(file_source[0])
                    index = edl["source"].index(source_group)
                    offset = source_frame - edl["shot_in"][index]
                    if offset < 0:
                        offset = 0
                    rvc.setFrame(edl["frame"][index] + offset)

        else:
            rvc.setViewNode("defaultSequence")
            rvc.setIntProperty(f"{self.sequence}.mode.useCutInfo", [1], True)
            rvc.setIntProperty(f"{self.sequence}.mode.autoEDL", [1], True)
            rvc.setIntProperty(f"{self.sequence}.output.autoSize", [0], True)
            rvc.setIntProperty(f"{self.sequence}.edl.frame", [], True)
            rvc.setIntProperty(f"{self.sequence}.edl.source", [], True)
            rvc.setIntProperty(f"{self.sequence}.edl.in", [], True)
            rvc.setIntProperty(f"{self.sequence}.edl.out", [], True)

        if return_signal:
            Signalling.get().rv_edl_loaded.emit()

    def connect_source(self, source_group):
        current_shots = rvc.nodeConnections(self.sequence_group) or []
        current_shots = current_shots and current_shots[0]
        if source_group not in current_shots:
            source = rv_commands.get_node_from_group(source_group, "RVFileSource")
            self.create_overlay("", source)
            rvc.setNodeInputs(self.sequence_group, current_shots + [source_group])

    def index_for_source(self, source_group):
        sources = rvc.nodeConnections(self.sequence_group)
        if sources:
            try:
                index = sources[0].index(source_group)
            except ValueError as e:
                return -1
            return index
        return -1

    def index_from_edl(self):
        frame_list = rvc.getIntProperty(f"{self.sequence}.edl.frame")
        for ii, frame in enumerate(frame_list):
            if rvc.frame() < frame:
                return ii - 1
        return -1

    def source_at_index(self, edl_index):
        frame_list = rvc.getIntProperty(f"{self.sequence}.edl.frame")
        if frame_list and len(frame_list) > edl_index:
            source_frame = frame_list[edl_index]
            file_source = rvc.sourcesAtFrame(source_frame)
            if file_source:
                return rvc.nodeGroup(file_source[0])

    def set_auto_edl(self, flag):
        rvc.setIntProperty(f"{self.sequence}.mode.autoEDL", [flag], True)
