from PySide2 import QtCore, QtWidgets

from ..ui.shot_widget import Ui_ShotWidget
from ..signalling import Signalling


class ShotWidget(QtWidgets.QWidget):
    close_me = QtCore.Signal(int)
    commit_data = QtCore.Signal(QtWidgets.QWidget)

    def __init__(self, item_id, parent):
        super(ShotWidget, self).__init__(parent)
        self._ui = Ui_ShotWidget()
        self._ui.setupUi(self)
        self.item_id = item_id
        self.can_close = True
        self.should_close = False
        self.original_shot_in = None
        self.original_shot_out = None
        self.current_shot_in = None
        self.current_shot_out = None
        self.block_min_and_max = True
        self.block_value_change = True
        self.show_handles = False

        self._ui.enabled_chk.stateChanged.connect(self._commit_data)

        self._ui.shotin_box.editing_start.connect(self._editing_started)
        self._ui.shotout_box.editing_start.connect(self._editing_started)
        self._ui.shotin_box.editingFinished.connect(self._editing_finished)
        self._ui.shotout_box.editingFinished.connect(self._editing_finished)
        self._ui.shotin_box.valueChanged.connect(self._update_shot_in)
        self._ui.shotout_box.valueChanged.connect(self._update_shot_out)
        self._ui.shotin_box.drag_changed.connect(self._drag_in_changed)
        self._ui.shotout_box.drag_changed.connect(self._drag_out_changed)

    def _commit_data(self):
        self.commit_data.emit(self)

    @property
    def shot_name(self):
        return self._ui.name_lbl.text()

    @shot_name.setter
    def shot_name(self, value):
        self._ui.name_lbl.setText(value)

    @property
    def order(self):
        return self._ui.order_lbl.text()

    @order.setter
    def order(self, value):
        self._ui.order_lbl.setText(str(value))

    @property
    def shot_in(self):
        return self._ui.shotin_box.value()

    @shot_in.setter
    def shot_in(self, value):
        self.current_shot_in = value
        self._ui.shotin_box.setValue(value)

    @property
    def shot_out(self):
        return self._ui.shotout_box.value()

    @shot_out.setter
    def shot_out(self, value):
        self.current_shot_out = value
        self._ui.shotout_box.setValue(value)

    @property
    def min_frame(self):
        return self._ui.shotin_box.minimum()

    @min_frame.setter
    def min_frame(self, value):
        self._ui.shotin_box.setMinimum(value)

    @property
    def max_frame(self):
        return self._ui.shotout_box.maximum()

    @max_frame.setter
    def max_frame(self, value):
        self._ui.shotout_box.setMaximum(value)

    @property
    def shot_in_changed(self):
        return self._ui.shotin_box.property("value_changed")

    @shot_in_changed.setter
    def shot_in_changed(self, value):
        if value != self.shot_in_changed:
            self._ui.shotin_box.setProperty("value_changed", value)
            self._ui.shotin_box.style().unpolish(self._ui.shotin_box)
            self._ui.shotin_box.ensurePolished()

    @property
    def shot_out_changed(self):
        return self._ui.shotout_box.property("value_changed")

    @shot_out_changed.setter
    def shot_out_changed(self, value):
        if value != self.shot_out_changed:
            self._ui.shotout_box.setProperty("value_changed", value)
            self._ui.shotout_box.style().unpolish(self._ui.shotout_box)
            self._ui.shotout_box.ensurePolished()

    @property
    def enabled(self):
        return self._ui.enabled_chk.isChecked()

    @enabled.setter
    def enabled(self, value):
        self._ui.enabled_chk.setChecked(value)

    @property
    def state(self):
        return self._ui.background.property("state")

    @state.setter
    def state(self, value):
        self._ui.background.setProperty("state", value)
        self._ui.background.style().unpolish(self._ui.background)
        self._ui.background.ensurePolished()

    def _update_shot_in(self, value):
        self.shot_in_changed = int(value) != int(self.original_shot_in)
        if self.shot_in_changed:
            if not self.block_min_and_max:
                self._ui.shotout_box.setMinimum(value)
            if not self.block_value_change:
                Signalling.get().set_source_frame.emit(value)

    def _update_shot_out(self, value):
        self.shot_out_changed = int(value) != int(self.original_shot_out)
        if self.shot_out_changed:
            if not self.block_min_and_max:
                self._ui.shotin_box.setMaximum(value)
            if not self.block_value_change:
                Signalling.get().set_source_frame.emit(value)

    def _editing_started(self):
        self.can_close = False

    def _editing_finished(self):
        self.can_close = True
        if self.should_close:
            self.close_me.emit(self.item_id)

    def _drag_in_changed(self, value):
        self.show_handles = value
        self._commit_data()
        Signalling.get().set_source_frame.emit(self.shot_in)

    def _drag_out_changed(self, value):
        self.show_handles = value
        self._commit_data()
        Signalling.get().set_source_frame.emit(self.shot_out)
