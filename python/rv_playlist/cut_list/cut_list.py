import json
import operator
from collections import Counter
from enum import IntEnum
from pathlib import Path

from PySide2 import QtWidgets, QtGui, QtCore

import sgtk

from edit_tools.rv_cut_data import RvCutData

import rv.commands as rvc

from ..rv_playlist import RvPlaylist
from ..signalling import Signalling
from .cut_model import CutModel, ItemDataRole, ItemType, MediaType
from .cut_filter_model import CutFilterModel
from .cut_delegate import CutDelegate


class MessageType(IntEnum):
    INFO = 0
    CRITICAL = 1


class CutList(QtWidgets.QListView):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.set_view_params()
        self._block_edl = False
        self._dragging = False
        self._handles = False

        self.data_model = CutModel(self)
        self.filter_model = CutFilterModel(self)

        self.filter_model.setSourceModel(self.data_model)
        self.setModel(self.filter_model)
        self.setItemDelegate(CutDelegate(self))
        self.setIconSize(QtCore.QSize(96, 34))

        self.selectionModel().selectionChanged.connect(self._selection_changed)
        self.customContextMenuRequested.connect(self.show_context_menu)

        Signalling.get().cut_list_update_current.connect(self.update_data_on_current)
        Signalling.get().rv_shot_changed.connect(self.change_shot)
        Signalling.get().alt_attribute_changed.connect(self.change_alt)
        Signalling.get().rv_loading_finished.connect(self.loading_finished)
        Signalling.get().request_edl.connect(self.get_edl)
        Signalling.get().rv_edl_loaded.connect(self.post_edl)
        Signalling.get().show_handles.connect(self.set_handles)
        Signalling.get().update_shot_from_qt.connect(self.update_rv_shot)
        Signalling.get().load_files.connect(self.load_cut)
        Signalling.get().clear_items.connect(self.clear)
        Signalling.get().set_to_movie.connect(self.set_all_to_movie)
        Signalling.get().set_to_stream.connect(self.set_all_to_stream)

    @property
    def block_edl(self):
        return self._block_edl

    @block_edl.setter
    def block_edl(self, value):
        self._block_edl = value

    @property
    def dragging(self):
        return self._dragging

    @dragging.setter
    def dragging(self, value):
        self._dragging = value

    def set_handles(self, value):
        self._handles = value
        self.get_edl(preserve_position=True)

    def update_data_on_current(self, data, role):
        current = self.currentIndex()
        self.model().setData(current, data, role)

    def _selection_changed(self, selected, deselected):
        if selected.indexes():
            for index in selected.indexes():
                item_id = index.data(ItemDataRole.ID_ROLE)
                if item_id in self.itemDelegate().editors:
                    self.itemDelegate().editors[item_id].state = 2
        else:
            index = self.currentIndex()
            item_id = index.data(ItemDataRole.ID_ROLE)
            if item_id in self.itemDelegate().editors:
                self.itemDelegate().editors[item_id].state = 1
        for index in deselected.indexes():
            item_id = index.data(ItemDataRole.ID_ROLE)
            if item_id in self.itemDelegate().editors:
                self.itemDelegate().editors[item_id].state = 0

    def clear_dialog(self):
        reply = QtWidgets.QMessageBox.question(
            None,
            "Clear Playlist",
            "Do you want to clear the playlist?",
            QtWidgets.QMessageBox.Yes,
            QtWidgets.QMessageBox.No,
        )

        if reply == QtWidgets.QMessageBox.Yes:
            self.clear()

    def clear(self):
        self.block_edl = True
        self.data_model.clear()
        self.filter_model.clear()
        Signalling.get().update_attribute_editor.emit(None)
        Signalling.get().clear_playlist.emit()
        self.block_edl = False

    def reset_range_on_selected(self):
        for selected_index in self.selectedIndexes():
            self.data_model.refresh_item(self.filter_model.mapToSource(selected_index))

    def refresh(self):
        self.data_model.refresh()

    def change_shot(self, cut_order):
        index = self.model().find_index_by_data(
            cut_order + 1, ItemDataRole.CUT_ORDER_ROLE
        )
        if index.isValid():
            self.setCurrentIndex(index)

    def loading_finished(self):
        self.data_model.process_pending_jobs()
        self.block_edl = False
        self.get_edl(return_signal=True)
        Signalling.get().load_finished.emit()

    def get_edl(self, return_signal=False, preserve_position=False):
        if self.block_edl:
            return
        start = None
        edl = {"frame": [], "source": [], "shot_in": [], "shot_out": []}
        for ii in range(self.model().rowCount()):
            index = self.model().index(ii, 0)
            if not index.data(ItemDataRole.ENABLED_ROLE):
                continue

            if start is None:
                start = (
                    index.data(ItemDataRole.DATA_ROLE).get("sequence_in", 1001)
                    + index.data(ItemDataRole.SHOT_IN_ROLE)
                    - index.data(ItemDataRole.DATA_ROLE).get(
                        "shot_in", index.data(ItemDataRole.SHOT_IN_ROLE)
                    )
                )

            edl["frame"].append(int(start))
            edl["source"].append(index.data(ItemDataRole.SOURCE_ROLE))

            if self._handles or index.data(ItemDataRole.SHOW_HANDLES_ROLE):
                shot_in = index.data(ItemDataRole.HANDLE_IN_ROLE)
                shot_out = index.data(ItemDataRole.HANDLE_OUT_ROLE)
            else:
                shot_in = index.data(ItemDataRole.SHOT_IN_ROLE)
                shot_out = index.data(ItemDataRole.SHOT_OUT_ROLE)

            edl["shot_in"].append(shot_in)
            edl["shot_out"].append(shot_out)
            start += shot_out - shot_in + 1

        edl["frame"].append(start)
        Signalling.get().new_edl.emit(edl, return_signal, preserve_position)

    def post_edl(self):
        new_current_index = (
            self.data_model.new_current and self.data_model.new_current.index()
        )
        if new_current_index:
            new_current_index = self.model().mapFromSource(new_current_index)
            self.selectionModel().setCurrentIndex(
                new_current_index, QtCore.QItemSelectionModel.ClearAndSelect
            )
            self.update_rv_shot(new_current_index)

    def update_rv_shot(self, index=None):
        if not index:
            index = self.currentIndex()
        if not index.isValid():
            return
        Signalling.get().update_shot.emit(
            index.data(),
            index.data(ItemDataRole.AUDIO_OFFSET_ROLE),
            index.data(ItemDataRole.CUT_ORDER_ROLE) - 1,
        )

    def remove_selected(self):
        ids_to_delete = []
        alt_ids_to_delete = []

        for selected_index in self.selectedIndexes():
            ids_to_delete.append(selected_index.data(ItemDataRole.ID_ROLE))

            alts = selected_index.data(ItemDataRole.ALTS_ROLE)
            if alts:
                alt_ids_to_delete.extend(list(alts.values()))

        Signalling.get().set_auto_edl.emit(1)
        Signalling.get().blockSignals(True)

        sources = []
        for id in alt_ids_to_delete + ids_to_delete:
            index = self.data_model.find_index_by_id(id)
            sources.append(index.data(ItemDataRole.SOURCE_ROLE))
            self.data_model.removeRow(index.row())

        for ii in range(self.model().rowCount()):
            index = self.model().index(ii, 0)
            index_source = index.data(ItemDataRole.SOURCE_ROLE)
            try:
                if index_source:
                    sources.remove(index_source)
            except ValueError:
                pass

        for source in sorted(sources):
            try:
                rvc.deleteNode(source)
            except Exception as e:
                RvPlaylist.app.logger.error(f"Source {source} could not be deleted")

        self.model().invalidateFilter()
        self.model().sort(0, True)

        Signalling.get().blockSignals(False)
        Signalling.get().set_auto_edl.emit(0)

        self.get_edl()

    def load_cut(self, data, start_order=None):
        if not start_order:
            current_index = self.currentIndex()
            if current_index and current_index.isValid():
                start_order = float(current_index.data(ItemDataRole.ORDER_ROLE))
            else:
                start_order = 0.0

        list_items = self._valid_items()

        Signalling.get().load_cut.emit(data, list_items, start_order)

    def _session_root(self):
        for i in range(self.model().rowCount()):
            index = self.model().index(i, 0)
            if index.isValid() and index.data(ItemDataRole.ENABLED_ROLE):
                mov_path = index.data(ItemDataRole.PATH_ROLE)
                mov_path = Path(mov_path)

                if mov_path.exists():
                    return str(mov_path.parent.parent.parent)

        # Needs code for a standard path for where artists can save

    def _valid_items(self):
        items = []

        for i in range(self.data_model.rowCount()):
            item = self.data_model.item(i)
            enabled = item.data(ItemDataRole.ENABLED_ROLE)
            item_type = item.data(ItemDataRole.TYPE_ROLE)

            if enabled and item_type == ItemType.CUT_ITEM_TYPE:
                items.append(item)

        return items

    def get_cut_data(self):
        list_items = self._valid_items()

        if not list_items:
            self._show_dialog(
                message_type=MessageType.CRITICAL,
                title="No Items",
                message="No valid items to proceed!",
            )
            return

        return RvCutData.from_rv(list_items, ItemDataRole, RvPlaylist.app)

    # EDITORS
    def close_editor(self, item_id=None, index=None):
        if not (item_id or index):
            return
        if not index:
            index = self.model().mapFromSource(
                self.data_model.find_index_by_id(item_id)
            )
        if not index.isValid():
            return
        if not item_id:
            item_id = index.data(ItemDataRole.ID_ROLE)
        editor = self.itemDelegate().editors.get(item_id)
        if editor:
            self.commitData(editor)
        if self.isPersistentEditorOpen(index):
            self.closePersistentEditor(index)

    def close_all_editors(self):
        for ii in range(self.model().rowCount()):
            self.close_editor(index=self.model().index(ii, 0))

    # ALTS
    def change_alt(self, new_alt_id):
        new_index = self.data_model.find_index_by_id(new_alt_id)
        previous_index = self.currentIndex()
        if not new_index.isValid() or not previous_index.isValid():
            return
        selected = self.selectionModel().isSelected(previous_index)
        self.close_all_editors()
        new_item = self.data_model.itemFromIndex(new_index)
        previous_item = self.data_model.itemFromIndex(
            self.filter_model.mapToSource(previous_index)
        )

        current_order = new_item.data(ItemDataRole.ORDER_ROLE)
        previous_order = previous_item.data(ItemDataRole.ORDER_ROLE)

        previous_item.setData(current_order, ItemDataRole.ORDER_ROLE)
        previous_item.setData(False, ItemDataRole.ACTIVE_ROLE)

        new_item.setData(True, ItemDataRole.ACTIVE_ROLE)
        new_item.setData(previous_order, ItemDataRole.ORDER_ROLE)

        self.model().invalidateFilter()
        self.model().sort(0, True)

        new_index = self.filter_model.mapFromSource(new_item.index())
        if selected:
            self.selectionModel().select(new_index, QtCore.QItemSelectionModel.Select)
        self.selectionModel().setCurrentIndex(
            new_index, QtCore.QItemSelectionModel.NoUpdate
        )

    # CONTEXT MENU
    def show_context_menu(self, pos):
        current = self.currentIndex()
        users = set([x["user"] for x in current.data(ItemDataRole.LOCAL_TAKES_ROLE)])

        menu = QtWidgets.QMenu(self)
        menu.addAction("Delete", self.remove_selected)
        menu.addAction("Latest Local", self.latest_local_action)
        user_data = menu.addMenu("Latest Local by User")
        for user in sorted(users):
            user_data.addAction(user, self.user_action)
        menu.addAction("Latest Published", self.latest_published_action)
        menu.addAction("Reset Frame Range", self.reset_range_on_selected)
        menu.addAction("To Movie", self.set_to_movie)
        frames_action = menu.addAction("To Frames", self.dummy)
        frames_action.setDisabled(True)
        menu.addAction("To Streaming", self.set_to_stream)
        menu.addAction("Clear Playlist", self.clear_dialog)

        menu.exec_(self.mapToGlobal(pos))

    def latest_local_action(self):
        current = self.currentIndex()
        local_versions = current.data(ItemDataRole.LOCAL_TAKES_ROLE)
        local_versions.sort(key=operator.itemgetter("timestamp"))
        self.model().setData(
            current, local_versions[-1], ItemDataRole.CURRENT_VERSION_ROLE
        )

    def latest_published_action(self):
        current = self.currentIndex()
        publish_versions = current.data(ItemDataRole.PUBLISHED_TAKES_ROLE)
        publish_versions.sort(key=operator.itemgetter("version"))
        self.model().setData(
            current, publish_versions[-1], ItemDataRole.CURRENT_VERSION_ROLE
        )

    def user_action(self):
        user = self.sender().text()
        current = self.currentIndex()
        local_versions = [
            x for x in current.data(ItemDataRole.LOCAL_TAKES_ROLE) if x["user"] == user
        ]
        local_versions.sort(key=operator.itemgetter("timestamp"))
        self.model().setData(
            current, local_versions[-1], ItemDataRole.CURRENT_VERSION_ROLE
        )

    def set_to_movie(self):
        indices = [self.filter_model.mapToSource(x) for x in self.selectedIndexes()]
        if indices:
            self.data_model.set_source_type(indices, MediaType.MOVIE)

    def set_to_stream(self):
        indices = [self.filter_model.mapToSource(x) for x in self.selectedIndexes()]
        if indices:
            self.data_model.set_source_type(indices, MediaType.STREAM)

    def set_all_to_movie(self):
        indices = []
        for ii in range(self.filter_model.rowCount()):
            index = self.filter_model.index(ii, 0)
            model_index = self.filter_model.mapToSource(index)
            indices.append(model_index)

        self.data_model.set_source_type(indices, MediaType.MOVIE)

    def set_all_to_stream(self):
        indices = []
        for ii in range(self.filter_model.rowCount()):
            index = self.filter_model.index(ii, 0)
            model_index = self.filter_model.mapToSource(index)
            indices.append(model_index)

        self.data_model.set_source_type(indices, MediaType.STREAM)

    # DRAG AND DROP
    def get_drop_position(self, index, pos):
        rect = self.rectForIndex(index)
        if pos.y() < rect.top() + rect.height() * 0.5:
            return QtWidgets.QAbstractItemView.AboveItem
        return QtWidgets.QAbstractItemView.BelowItem

    def render_to_pixmap(self, indexes):
        index = indexes[0]
        delegate = self.itemDelegate(index)
        options = self.view_options()
        size = options.decorationSize
        options.rect = QtCore.QRect(0, 0, size.width(), size.height())
        pixmap = QtGui.QPixmap(size)
        pixmap.fill(QtCore.Qt.white)
        painter = QtGui.QPainter(pixmap)
        delegate.paint(painter, options, index)
        return pixmap

    def view_options(self):
        options = QtWidgets.QStyleOptionViewItem()
        options.initFrom(self)
        options.state &= ~QtWidgets.QStyle.State_MouseOver
        options.font = self.font()
        if not self.hasFocus():
            options.state &= ~QtWidgets.QStyle.State_Active
        options.state &= ~QtWidgets.QStyle.State_HasFocus
        icon_size = self.iconSize()
        icon_size.setWidth(self.width())
        options.decorationSize = icon_size
        options.decorationPosition = QtWidgets.QStyleOptionViewItem.Left
        options.decorationAlignment = QtCore.Qt.AlignCenter
        options.displayAlignment = QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter
        options.textElideMode = self.textElideMode()
        options.rect = QtCore.QRect()
        options.showDecorationSelected = self.style().styleHint(
            QtWidgets.QStyle.SH_ItemView_ShowDecorationSelected
        )
        options.locale = self.locale()
        options.locale.setNumberOptions(QtCore.QLocale.OmitGroupSeparator)
        options.widget = self
        return options

    def set_view_params(self):
        self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.setDragEnabled(True)
        self.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)
        self.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.setAlternatingRowColors(True)
        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.setAcceptDrops(True)

    # GENERAL OVERRIDES
    def currentChanged(self, current, previous):
        result = super().currentChanged(current, previous)
        if not current.isValid():
            return result
        Signalling.get().update_attribute_editor.emit(current)
        cut_order = current.data(ItemDataRole.CUT_ORDER_ROLE)
        if cut_order and cut_order != "-":
            Signalling.get().qt_shot_changed.emit(
                current.data(),
                current.data(ItemDataRole.AUDIO_OFFSET_ROLE),
                cut_order - 1,
            )
        return result

    def dummy(self):
        pass

    def _show_dialog(self, message_type=MessageType.INFO, title="", message=""):
        func = QtWidgets.QMessageBox.information

        if message_type == MessageType.CRITICAL:
            func = QtWidgets.QMessageBox.critical

        func(
            None,
            title,
            message,
            QtWidgets.QMessageBox.Ok,
        )

    # INTERNAL REORDER OVERRIDES
    def startDrag(self, supported_actions):
        indexes = self.selectedIndexes()
        if len(indexes) > 0:
            try:
                self.block_edl = True
                items = []
                for index in indexes:
                    source_index = self.filter_model.mapToSource(index)
                    items.append(self.data_model.itemFromIndex(source_index))
                data = self.model().mimeData(indexes)
                if not data:
                    return
                pixmap = self.render_to_pixmap(indexes)
                rect = pixmap.rect()
                self.selectionModel().clearCurrentIndex()
                for item in items:
                    item.setData(False, ItemDataRole.ACTIVE_ROLE)
                self.model().invalidateFilter()
                pos = self.mapFromGlobal(QtGui.QCursor.pos())
                pos.setY(rect.height() - (pos.y() % rect.height()))
                drag = QtGui.QDrag(self)
                drag.setMimeData(data)
                drag.setPixmap(pixmap)
                drag.setHotSpot(pos + rect.topLeft())
                drag.exec_()
                for item in items:
                    item.setData(True, ItemDataRole.ACTIVE_ROLE)

                if len(items) > 1:
                    order = items[0].data(ItemDataRole.ORDER_ROLE) + 0.51
                    for item in items[1:]:
                        item.setData(order, ItemDataRole.ORDER_ROLE)
                        order += 0.01

            finally:
                self.block_edl = False

            self.model().invalidateFilter()
            self.model().sort(0, True)
            new_current = self.filter_model.mapFromSource(items[0].index())
            selection = QtCore.QItemSelection(
                new_current,
                self.filter_model.mapFromSource(items[-1].index()),
            )
            self.selectionModel().select(
                selection, QtCore.QItemSelectionModel.ClearAndSelect
            )
            self.setCurrentIndex(new_current)

    # EDITOR OVERRIDES
    def mouseMoveEvent(self, event):
        index = self.indexAt(event.pos())

        if index.isValid() and not self.dragging:
            for ii in range(self.model().rowCount()):
                if index.row() == ii:
                    continue
                other_index = self.model().index(ii, 0)
                if other_index.data(ItemDataRole.TYPE_ROLE) == ItemType.INDICATOR_TYPE:
                    continue
                if self.isPersistentEditorOpen(other_index):
                    other_id = other_index.data(ItemDataRole.ID_ROLE)
                    if other_id in self.itemDelegate().editors:
                        if self.itemDelegate().editors[other_id].can_close:
                            self.close_editor(index=other_index)
                        else:
                            self.itemDelegate().editors[other_id].should_close = True
            if index.data(ItemDataRole.TYPE_ROLE) == ItemType.CUT_ITEM_TYPE:
                if self.isPersistentEditorOpen(index):
                    item_id = index.data(ItemDataRole.ID_ROLE)
                    if item_id in self.itemDelegate().editors:
                        self.itemDelegate().editors[item_id].should_close = False
                else:
                    self.openPersistentEditor(index)

        return super().mouseMoveEvent(event)

    # DRAG DROP OVERRIDES
    def dragEnterEvent(self, event):
        self.dragging = True
        self.block_edl = True
        if (
            event.mimeData().hasUrls()
            or event.mimeData().data("cut_items/list")
            or event.mimeData().data("versions/list")
        ):
            event.acceptProposedAction()
        else:
            return super().dragEnterEvent(event)

    def dragLeaveEvent(self, event):
        self.dragging = False
        self.block_edl = False
        self.data_model.remove_indicator_item()
        return super().dragLeaveEvent(event)

    def dragMoveEvent(self, event):
        super().dragMoveEvent(event)
        index = self.indexAt(event.pos())
        position = self.get_drop_position(index, event.pos())
        self.data_model.create_indicator_item()

        if index.isValid():
            index_order = index.data(ItemDataRole.ORDER_ROLE)
        else:
            index_order = self.filter_model.get_last_position()
        if not index_order % 1:
            if position == QtWidgets.QAbstractItemView.AboveItem:
                new_order = index_order - 0.5
            else:
                new_order = index_order + 0.5
            current_order = self.data_model.indicator_item.data(ItemDataRole.ORDER_ROLE)
            if current_order != new_order:
                self.data_model.indicator_item.setData(
                    new_order, ItemDataRole.ORDER_ROLE
                )
                self.filter_model.sort(0)

        return True

    def dropEvent(self, event):
        self.dragging = False
        if (
            event.mimeData().hasUrls()
            or event.mimeData().data("cut_items/list")
            or event.mimeData().data("versions/list")
        ):
            data = []
            start_order = 1.0
            if self.data_model.indicator_item:
                start_order = self.data_model.indicator_item.data(
                    ItemDataRole.ORDER_ROLE
                )
            if event.mimeData().hasUrls():
                data = [x.toLocalFile() for x in event.mimeData().urls()]
            else:
                if event.mimeData().data("cut_items/list"):
                    data = json.loads(
                        str(event.mimeData().data("cut_items/list"), "utf-8")
                    )
                elif event.mimeData().data("versions/list"):
                    data = json.loads(
                        str(event.mimeData().data("versions/list"), "utf-8")
                    )

            self.load_cut(data, start_order)
        else:
            super(CutList, self).dropEvent(event)

        self.block_edl = False

    def check_shot_names(self):
        invalid_names = []
        shot_names = Counter(
            [item.data(ItemDataRole.DISPLAY_ROLE) for item in self._valid_items()]
        )

        for shot_name in shot_names:
            if "-" in shot_name:
                invalid_names.append(shot_name)

        error_msg = ""

        if invalid_names:
            error_msg += (
                "Shot names are indexed and need to be renamed!\n"
                + "\n".join(invalid_names)
                + "\n\n"
            )

        duplicate_names = [
            shot_name for shot_name in shot_names if shot_names[shot_name] > 1
        ]
        if duplicate_names:
            error_msg += "Duplicate shot names need to be renamed!\n" + "\n".join(
                duplicate_names
            )

        if error_msg:
            self._show_dialog(
                message_type=MessageType.CRITICAL,
                title="Incorrect shot names!",
                message=error_msg,
            )
            return True

        return False

    def save(self):
        if self.check_shot_names():
            return

        desc, proceed = QtWidgets.QInputDialog.getText(
            self, "Enter description", "Enter a brief description for the cut..."
        )
        if not proceed:
            return

        rv_cut_data = self.get_cut_data()
        if not rv_cut_data:
            return

        try:
            rv_cut_data.save_custom(desc)
        except Exception as e:
            import traceback

            print(traceback.format_exc())

            self._show_dialog(
                message_type=MessageType.CRITICAL,
                title="Cut Save Failed",
                message=f"Cut failed to save with error\n{e}!",
            )

        else:
            self._show_dialog(
                message_type=MessageType.INFO,
                title="Cut Saved",
                message="Cut was saved successfully",
            )

    def publish(self):
        if self.check_shot_names():
            return

        rv_cut_data = self.get_cut_data()
        if not rv_cut_data:
            return

        publish_settings = QtCore.QSettings("Locksmith Animation", RvPlaylist.app.name)

        publish_form = get_publish_description(publish_settings, rv_cut_data.is_cut)
        if publish_form.cancelled:
            return

        rv_cut_data.publisher = publish_form.publish_cut
        rv_cut_data.publish_desc = publish_form.description

        from .async_publish import AsyncPublish

        AsyncPublish(self, rv_cut_data)


def get_publish_description(publish_settings, is_previs):
    from ..publish_form import RvPublishForm

    publish_ui = RvPublishForm(publish_settings)
    publish_ui.ui.cut_publish_radio.setChecked(is_previs)
    publish_ui.ui.playlist_publish_radio.setChecked(not is_previs)
    publish_ui.exec_()

    return publish_ui


# class DescDialog(QtWidgets.QWidget):
#     def __init__(self, parent=None):
#         super().__init__(parent=parent)
#         self.setGeometry(400, 50, 200, 200)

#         self.
#         self.pushButton = QtWidgets.QPushButton("Ok", self)
#         self.pushButton.setGeometry(25, 90, 150, 25)
#         self.pushButton.clicked.connect(self.close)
