from tank.platform.qt import QtCore, QtGui

from .shot_widget import ShotWidget
from .cut_model import ItemDataRole, ItemType


class CutDelegate(QtGui.QStyledItemDelegate):
    def __init__(self, parent):
        super().__init__(parent)
        self._paint_widget = None
        self.editors = {}

    @property
    def paint_widget(self):
        if not self._paint_widget:
            self._paint_widget = ShotWidget(-1, self.parent())
        return self._paint_widget

    def get_state(self, index):
        return (
            (index.row() in [x.row() for x in self.parent().selectedIndexes()] and 2)
            or (self.parent().currentIndex() == index and 1)
            or 0
        )

    def createEditor(self, parent, option, index):
        item_id = index.data(ItemDataRole.ID_ROLE)
        self.editors[item_id] = ShotWidget(item_id, parent)
        self.editors[item_id].block_min_and_max = False
        self.editors[item_id].commit_data.connect(self.commitData.emit)
        self.editors[item_id].close_me.connect(self.parent().close_editor)
        return self.editors[item_id]

    def destroyEditor(self, editor, index):
        item_id = index.data(ItemDataRole.ID_ROLE)
        if item_id in self.editors:
            del self.editors[item_id]
        return super().destroyEditor(editor, index)

    def updateEditorGeometry(self, editor, option, index):
        editor.setGeometry(option.rect)

    def setEditorData(self, editor, index):
        editor.block_value_change = True
        editor.shot_name = index.data(QtCore.Qt.DisplayRole)
        editor.order = index.data(ItemDataRole.CUT_ORDER_ROLE)
        editor.original_shot_in = index.data(ItemDataRole.DATA_ROLE).get("shot_in")
        editor.original_shot_out = index.data(ItemDataRole.DATA_ROLE).get("shot_out")
        editor.shot_in = index.data(ItemDataRole.SHOT_IN_ROLE)
        editor.shot_out = index.data(ItemDataRole.SHOT_OUT_ROLE)
        editor.min_frame = index.data(ItemDataRole.HANDLE_IN_ROLE)
        editor.max_frame = index.data(ItemDataRole.HANDLE_OUT_ROLE)
        editor.enabled = index.data(ItemDataRole.ENABLED_ROLE)
        editor.show_handles = index.data(ItemDataRole.SHOW_HANDLES_ROLE)
        editor.state = self.get_state(index)
        editor.block_value_change = False

    def setModelData(self, editor, model, index):
        model.setItemData(
            index,
            {
                ItemDataRole.SHOW_HANDLES_ROLE: editor.show_handles,
                ItemDataRole.SHOT_IN_ROLE: editor.shot_in,
                ItemDataRole.SHOT_OUT_ROLE: editor.shot_out,
                ItemDataRole.ENABLED_ROLE: editor.enabled,
            },
        )

    def paint(self, painter, option, index):
        painter.save()
        try:
            painter.translate(option.rect.topLeft())
            if index.data(ItemDataRole.TYPE_ROLE) == ItemType.CUT_ITEM_TYPE:
                self.paint_widget.shot_name = index.data(QtCore.Qt.DisplayRole)
                self.paint_widget.order = index.data(ItemDataRole.CUT_ORDER_ROLE)
                self.paint_widget.original_shot_in = index.data(
                    ItemDataRole.DATA_ROLE
                ).get("shot_in")
                self.paint_widget.original_shot_out = index.data(
                    ItemDataRole.DATA_ROLE
                ).get("shot_out")
                self.paint_widget.shot_in = index.data(ItemDataRole.SHOT_IN_ROLE)
                self.paint_widget.shot_out = index.data(ItemDataRole.SHOT_OUT_ROLE)
                self.paint_widget.enabled = index.data(ItemDataRole.ENABLED_ROLE)
                self.paint_widget.state = self.get_state(index)

                self.paint_widget.resize(option.rect.size())
                self.paint_widget.render(
                    painter,
                    QtCore.QPoint(0, 0),
                    renderFlags=QtGui.QWidget.DrawChildren,
                )
        finally:
            painter.restore()

    def sizeHint(self, option, index):
        if index.data(ItemDataRole.TYPE_ROLE) == ItemType.INDICATOR_TYPE:
            return QtCore.QSize(96, 17)

        return self.parent().iconSize()
