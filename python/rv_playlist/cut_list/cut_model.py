import re
import time
from enum import IntEnum
from pathlib import Path

from PySide2 import QtGui, QtCore

from edit_tools.rv_cut_data import RvCutData

from .find_model_mixin import FindModelMixin
from ..rv_playlist import RvPlaylist
from ..signalling import Signalling
from ..data_loader import calculate_cut_audio


def increment_shot_name(shot_name, shot_names_in_take):
    regex_match = re.match(
        "([a-z]\d{4})_(pv)?(\d{4})(_c$)?(_[ps]\d{2})?([a-z]?)$", shot_name
    )
    seq, _pv, shot_num, _c, _ps_num, alt_char = regex_match.groups()
    _pv = _pv or ""
    _c = _c or ""
    _ps_num = _ps_num or ""
    alt_char = alt_char or ""

    while shot_name in shot_names_in_take:
        shot_num = int(shot_num) + 1
        shot_num = "{:04d}".format(shot_num)
        shot_name = f"{seq}_{_pv}{shot_num}{_c}{_ps_num}{alt_char}"

    return shot_name


class ItemDataRole(IntEnum):
    LOCAL_TAKES_ROLE = QtCore.Qt.UserRole + 1
    PUBLISHED_TAKES_ROLE = QtCore.Qt.UserRole + 2
    CURRENT_VERSION_ROLE = QtCore.Qt.UserRole + 3
    STEP_ROLE = QtCore.Qt.UserRole + 5
    ORDER_ROLE = QtCore.Qt.UserRole + 6
    SHOT_IN_ROLE = QtCore.Qt.UserRole + 7
    SHOT_OUT_ROLE = QtCore.Qt.UserRole + 8
    ENABLED_ROLE = QtCore.Qt.UserRole + 9
    SOURCE_ROLE = QtCore.Qt.UserRole + 10
    AUDIO_OFFSET_ROLE = QtCore.Qt.UserRole + 11
    DATA_ROLE = QtCore.Qt.UserRole + 12
    TYPE_ROLE = QtCore.Qt.UserRole + 13
    PATH_ROLE = QtCore.Qt.UserRole + 14
    ALTS_ROLE = QtCore.Qt.UserRole + 15
    CURRENT_ALT_ROLE = QtCore.Qt.UserRole + 16
    ACTIVE_ROLE = QtCore.Qt.UserRole + 17
    SG_SHOT_ROLE = QtCore.Qt.UserRole + 18
    HANDLE_IN_ROLE = QtCore.Qt.UserRole + 19
    HANDLE_OUT_ROLE = QtCore.Qt.UserRole + 20
    SHOW_HANDLES_ROLE = QtCore.Qt.UserRole + 21
    CUT_ORDER_ROLE = QtCore.Qt.UserRole + 22
    ID_ROLE = QtCore.Qt.UserRole + 23
    AUDIO_PATH_ROLE = QtCore.Qt.UserRole + 24
    REFERENCE_SHOT_ROLE = QtCore.Qt.UserRole + 25
    STREAM_ROLE = QtCore.Qt.UserRole + 26
    SOURCE_TYPE_ROLE = QtCore.Qt.UserRole + 27
    DISPLAY_ROLE = QtCore.Qt.DisplayRole


class ItemType(IntEnum):
    CUT_ITEM_TYPE = 1
    INDICATOR_TYPE = 2


class MediaType(IntEnum):
    MOVIE = 0
    STREAM = 1
    FRAMES = 2


class CutModel(QtGui.QStandardItemModel, FindModelMixin):
    STREAM_ADDRESS = "https://locksmith.shotgunstudio.com/file_serve/version/{}/mp4"

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.indicator_item = None
        self.new_current = None
        self.pending_jobs = []

        self.dataChanged.connect(self.data_changed)

        Signalling.get().new_cut_items.connect(self.load_items)
        Signalling.get().new_source_group.connect(self.new_source_group)

    def sort_drop(self):
        self.remove_indicator_item()
        all_items = []

        for ii in range(self.rowCount()):
            all_items.append(self.item(ii))

        all_items = sorted(all_items, key=lambda x: x.data(ItemDataRole.ORDER_ROLE))
        all_items = sorted(
            all_items, key=lambda x: int(x.data(ItemDataRole.ACTIVE_ROLE)), reverse=True
        )

        for ii, item in enumerate(all_items):
            item.setData(ii + 1, ItemDataRole.ORDER_ROLE)

    def generate_random_id(self, offset):
        return int(time.time() % 10000 * 100000) + (offset * 1000000)

    def data_changed(self, top_left, bottom_right, roles):
        if ItemDataRole.ENABLED_ROLE in roles:
            self.parent().model().sort(0, True)  # move this
        if ItemDataRole.CURRENT_VERSION_ROLE in roles:
            current_version = top_left.data(ItemDataRole.CURRENT_VERSION_ROLE)
            self.setData(top_left, current_version["path"], ItemDataRole.PATH_ROLE)
            self.setData(
                top_left, current_version["take_in"], ItemDataRole.HANDLE_IN_ROLE
            )
            self.setData(
                top_left, current_version["take_out"], ItemDataRole.HANDLE_OUT_ROLE
            )
            self.setData(
                top_left,
                max(
                    current_version["take_in"], top_left.data(ItemDataRole.SHOT_IN_ROLE)
                ),
                ItemDataRole.SHOT_IN_ROLE,
            )
            self.setData(
                top_left,
                min(
                    current_version["take_out"],
                    top_left.data(ItemDataRole.SHOT_OUT_ROLE),
                ),
                ItemDataRole.SHOT_OUT_ROLE,
            )
            if current_version["sg_id"]:
                cut_item = RvPlaylist.app.shotgun.find_one(
                    "CutItem",
                    [
                        [
                            "version",
                            "is",
                            {"type": "Version", "id": current_version["sg_id"]},
                        ]
                    ],
                    ["cut"],
                )
                cut_data = RvCutData.from_cut_items(
                    [cut_item],
                    RvPlaylist.app.shotgun,
                )
            else:
                cut_data = RvCutData.from_mov(current_version["path"], RvPlaylist.app)
            if current_version["has_audio"]:
                self.setData(top_left, 0, ItemDataRole.AUDIO_OFFSET_ROLE)
                self.setData(top_left, None, ItemDataRole.AUDIO_PATH_ROLE)
            else:
                calculate_cut_audio(cut_data)
                self.setData(
                    top_left,
                    cut_data.shots[0].audio_offset,
                    ItemDataRole.AUDIO_OFFSET_ROLE,
                )
                self.setData(
                    top_left, cut_data.takes[0].audio, ItemDataRole.AUDIO_PATH_ROLE
                )
            self.setData(
                top_left, cut_data.shots[0].original_data, ItemDataRole.DATA_ROLE
            )
            self.load_sources([top_left])
            Signalling.get().update_attribute_editor.emit(top_left)

    def process_pending_jobs(self):
        for _ in range(len(self.pending_jobs)):
            pending_job = self.pending_jobs.pop(0)
            pending_job["func"](*pending_job["args"])

    def get_current_take(self, shot):
        current_version = None
        for version in shot.take.local_versions + shot.take.published_versions:
            if not shot.take:
                continue

            if Path(shot.take.mov_path) == Path(version["path"]):
                current_version = version
                break

        if not current_version:
            if shot.take.local_versions:
                current_version = shot.take.local_versions[0]
            elif shot.take.published_versions:
                current_version = shot.take.published_versions[0]

        return current_version

    def load_items(self, cut_data):
        Signalling.get().blockSignals(True)
        self.new_current = None
        uids = {}

        for ii, shot in enumerate(cut_data.shots):
            uid = self.generate_random_id(ii)
            uids[shot.name] = shot.uid = uid

        for shot in cut_data.shots:
            if not shot.take:
                raise Exception(
                    f"Shot {shot.name} does not have a take associated with it"
                )

            item = QtGui.QStandardItem(shot.name)
            item.setData(shot.shot_in, ItemDataRole.SHOT_IN_ROLE)
            item.setData(shot.shot_out, ItemDataRole.SHOT_OUT_ROLE)
            item.setData(False, ItemDataRole.SHOW_HANDLES_ROLE)
            item.setData(True, ItemDataRole.ENABLED_ROLE)
            item.setData(ItemType.CUT_ITEM_TYPE, ItemDataRole.TYPE_ROLE)
            item.setData(shot.active_shot, ItemDataRole.ACTIVE_ROLE)
            item.setData({x.name: x.uid for x in shot.alts}, ItemDataRole.ALTS_ROLE)
            item.setData(shot.order, ItemDataRole.CUT_ORDER_ROLE)
            item.setData(shot.order, ItemDataRole.ORDER_ROLE)
            item.setData(shot.uid, ItemDataRole.ID_ROLE)
            item.setData(shot.audio_offset, ItemDataRole.AUDIO_OFFSET_ROLE)
            item.setData(shot.original_data, ItemDataRole.DATA_ROLE)
            item.setData(shot.reference_shot, ItemDataRole.REFERENCE_SHOT_ROLE)
            item.setData(MediaType.MOVIE, ItemDataRole.SOURCE_TYPE_ROLE)
            item.setFlags(item.flags() & ~QtCore.Qt.ItemIsDropEnabled)
            item.setData(shot.take.take_in, ItemDataRole.HANDLE_IN_ROLE)
            item.setData(shot.take.take_out, ItemDataRole.HANDLE_OUT_ROLE)
            item.setData(shot.take.mov_path, ItemDataRole.PATH_ROLE)
            item.setData(shot.take.audio, ItemDataRole.AUDIO_PATH_ROLE)
            item.setData(shot.take and shot.take.step, ItemDataRole.STEP_ROLE)
            item.setData(shot.take.local_versions, ItemDataRole.LOCAL_TAKES_ROLE)
            item.setData(
                shot.take.published_versions, ItemDataRole.PUBLISHED_TAKES_ROLE
            )

            for version in shot.take.published_versions:
                if Path(shot.take.mov_path) == Path(version["path"]):
                    item.setData(version, ItemDataRole.CURRENT_VERSION_ROLE)
                    item.setData(
                        self.STREAM_ADDRESS.format(version["sg_id"]),
                        ItemDataRole.STREAM_ROLE,
                    )
                    break

            for version in shot.take.local_versions:
                if Path(shot.take.mov_path) == Path(version["path"]):
                    item.setData(version, ItemDataRole.CURRENT_VERSION_ROLE)
                    item.setData(None, ItemDataRole.STREAM_ROLE)
                    break

            self.appendRow(item)
            if not self.new_current:
                self.new_current = item
            Signalling.get().update_progress.emit(1)

        self.sort_drop()
        self.parent().model().invalidateFilter()
        self.parent().model().sort(0, True)

        Signalling.get().blockSignals(False)

        self.load_sources()

    def load_sources(self, indices=None):
        if not indices:
            indices = []
            for ii in range(self.rowCount()):
                indices.append(self.index(ii, 0))
        cut_data = {}
        for index in indices:
            if index.data(ItemDataRole.SOURCE_TYPE_ROLE) == MediaType.MOVIE:
                path = index.data(ItemDataRole.PATH_ROLE)
            elif index.data(ItemDataRole.SOURCE_TYPE_ROLE) == MediaType.STREAM:
                if index.data(ItemDataRole.STREAM_ROLE):
                    path = index.data(ItemDataRole.STREAM_ROLE)
                else:
                    self.setData(index, MediaType.MOVIE, ItemDataRole.SOURCE_TYPE_ROLE)
                    path = index.data(ItemDataRole.PATH_ROLE)

            if path not in cut_data:
                cut_data[path] = {
                    "path": path,
                    "take_in": index.data(ItemDataRole.HANDLE_IN_ROLE),
                    "audio_path": index.data(ItemDataRole.AUDIO_PATH_ROLE),
                    "audio_offset": index.data(ItemDataRole.AUDIO_OFFSET_ROLE),
                    "uids": [],
                }
            cut_data[path]["uids"].append(index.data(ItemDataRole.ID_ROLE))

        Signalling.get().load_sources.emit(cut_data)

    def set_source_type(self, indices, source_type):
        sources_to_remove = []
        indices_to_load = []
        for index in indices:
            if index.isValid():
                if self.data(index, ItemDataRole.SOURCE_TYPE_ROLE) == source_type:
                    continue
                if self.data(index, ItemDataRole.SOURCE_TYPE_ROLE) == MediaType.STREAM:
                    if not self.data(index, ItemDataRole.STREAM_ROLE):
                        continue
                indices_to_load.append(index)
                self.setData(index, source_type, ItemDataRole.SOURCE_TYPE_ROLE)
                source_group = self.data(index, ItemDataRole.SOURCE_ROLE)
                if source_group:
                    sources_to_remove.append(source_group)
                self.setData(index, None, ItemDataRole.SOURCE_ROLE)
        self.pending_jobs.append(
            {"func": Signalling.get().delete_sources.emit, "args": [sources_to_remove]}
        )
        self.load_sources(indices)

    def to_quicktime(self, indices):
        self.set_source_type(indices, 0)

    def to_stream(self, indices):
        self.set_source_type(indices, 1)

    def new_source_group(self, source_group, uids):
        for uid in uids:
            index = self.find_index_by_id(uid)
            if index.isValid():
                self.setData(index, source_group, ItemDataRole.SOURCE_ROLE)

    def create_indicator_item(self):
        if self.indicator_item:
            return
        self.indicator_item = QtGui.QStandardItem()
        self.indicator_item.setData(True, ItemDataRole.ACTIVE_ROLE)
        self.indicator_item.setFlags(QtCore.Qt.NoItemFlags)
        self.indicator_item.setData(self.rowCount() + 10, ItemDataRole.ORDER_ROLE)
        self.indicator_item.setData(ItemType.INDICATOR_TYPE, ItemDataRole.TYPE_ROLE)
        self.appendRow(self.indicator_item)

    def remove_indicator_item(self):
        if self.indicator_item:
            self.removeRow(self.indicator_item.row())
            self.indicator_item = None

    def find_index_by_id(self, item_id):
        return self.find_index_by_data(item_id, ItemDataRole.ID_ROLE)

    def refresh_item(self, index):
        self.setData(
            index,
            # index.data(ItemDataRole.SHOT_IN_ROLE)
            # or index.data(ItemDataRole.DATA_ROLE).get("shot_in"),
            index.data(ItemDataRole.DATA_ROLE).get("shot_in"),
            ItemDataRole.SHOT_IN_ROLE,
        )

        self.setData(
            index,
            # index.data(ItemDataRole.SHOT_OUT_ROLE)
            # or index.data(ItemDataRole.DATA_ROLE).get("shot_out"),
            index.data(ItemDataRole.DATA_ROLE).get("shot_out"),
            ItemDataRole.SHOT_OUT_ROLE,
        )

    def refresh(self):
        for ii in range(self.rowCount()):
            index = self.index(ii, 0)
            self.refresh_item(index)

    def dropMimeData(self, data, action, row, column, parent):
        if not data or not (
            action == QtCore.Qt.CopyAction or action == QtCore.Qt.MoveAction
        ):
            return False
        mime_format = "application/x-qabstractitemmodeldatalist"
        if not data.hasFormat(mime_format):
            return super().dropMimeData(data, action, row, column, parent)

        encoded = QtCore.QByteArray(data.data(mime_format))
        stream = QtCore.QDataStream(encoded)
        original_row = stream.readInt32()
        index = self.index(original_row, 0)
        self.setData(
            index,
            self.indicator_item.data(ItemDataRole.ORDER_ROLE),
            ItemDataRole.ORDER_ROLE,
        )
        self.setData(index, True, ItemDataRole.ACTIVE_ROLE)
        self.sort_drop()
        return True

    def clear(self):
        self.indicator_item = None
        self.new_current = None

        return super().clear()
