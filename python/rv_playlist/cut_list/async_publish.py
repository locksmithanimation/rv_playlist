import traceback

from tank.platform.qt import QtCore, QtGui

from ..rv_playlist import RvPlaylist


class AsyncPublish(QtCore.QObject):
    def __init__(self, parent, rv_cut_data):
        super(AsyncPublish, self).__init__(parent)
        self.publish_thread = QtCore.QThread(self)
        self.progress_dialog = QtGui.QProgressDialog(
            f"Publish {rv_cut_data.publish_kind}",
            None,
            0,
            100,
            self.parent(),
        )

        self.progress_dialog.setWindowTitle(f"Publishing {rv_cut_data.publish_kind}")
        self.progress_dialog.setAutoClose(True)
        self.progress_dialog.setAutoReset(True)
        self.progress_dialog.setMinimumDuration(0)

        self.publish_worker = PublishWorker(rv_cut_data)
        self.publish_worker.moveToThread(self.publish_thread)
        self.publish_worker.error.connect(self.log_error)
        self.publish_worker.done.connect(self.work_done)
        self.publish_worker.finish.connect(self.cleanup_thread)
        self.publish_worker.progress_updated.connect(self.update_progress)
        self.publish_thread.started.connect(self.publish_worker.do_work)
        self.progress_dialog.setValue(0)
        self.publish_thread.start()

    def update_progress(self, value):
        self.progress_dialog.setValue(int(value))

    def log_error(self, error):
        error_message = traceback.format_exception(
            type(error), error, error.__traceback__
        )
        error_message = "".join(error_message)
        RvPlaylist.app.logger.error(error_message)
        QtGui.QMessageBox.critical(self.parent(), "Error", error_message)
        raise error

    def work_done(self):
        if self.publish_worker.existing_publishes:
            versions_info = ""

            for publish in self.publish_worker.existing_publishes:
                versions_info += f"Version Name: {publish['version.Version.code']}"
                versions_info += "\n"

            QtGui.QMessageBox.information(
                self.parent(),
                "Published versions found!",
                "Versions with this name have been published before. They will not be published again,\n\n"
                + versions_info,
            )

        else:
            QtGui.QMessageBox.information(
                self.parent(),
                "Publish Complete",
                "The publish completed successfully.",
            )

    def cleanup_thread(self):
        self.publish_thread.exit()
        self.publish_thread.deleteLater()
        self.publish_worker = None


class PublishWorker(QtCore.QObject):
    progress_updated = QtCore.Signal(int)
    done = QtCore.Signal()
    finish = QtCore.Signal()
    error = QtCore.Signal(Exception)

    def __init__(self, rv_cut_data):
        super().__init__()

        self.rv_cut_data = rv_cut_data
        self.existing_publishes = []

    def do_work(self):
        try:
            publisher = self.rv_cut_data.publish(self.update_progress)
            self.existing_publishes = publisher.existing_publishes

            self.done.emit()

        except Exception as e:
            self.progress_updated.emit(100)
            self.error.emit(e)

        finally:
            self.finish.emit()

    def update_progress(self, value):
        self.progress_updated.emit(value)
