from PySide2 import QtCore


class FindModelMixin(object):
    def find_index_by_data(self, data, role):
        indices = self.match(self.index(0, 0), role, data)
        if indices:
            return indices[0]
        return QtCore.QModelIndex()