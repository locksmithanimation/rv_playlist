from PySide2 import QtCore

from .cut_model import ItemDataRole, ItemType
from .find_model_mixin import FindModelMixin
from ..rv_playlist import RvPlaylist
from ..signalling import Signalling


class CutFilterModel(QtCore.QSortFilterProxyModel, FindModelMixin):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.setDynamicSortFilter(False)
        self._show_alts = False

        self.setSortRole(ItemDataRole.ORDER_ROLE)

        self.rowsInserted.connect(self.rows_inserted)
        self.rowsRemoved.connect(self.rows_removed)
        self.dataChanged.connect(self.data_changed)

        Signalling.get().show_alts.connect(self.show_alts)

    def sort(self, column, update_data=False):
        result = super().sort(column)
        if update_data:
            count = 1
            for ii in range(self.rowCount()):
                index = self.index(ii, 0)
                if not index.data(ItemDataRole.ENABLED_ROLE):
                    cut_order = "-"
                else:
                    cut_order = count
                    count += 1
                self.setData(index, cut_order, ItemDataRole.CUT_ORDER_ROLE)
        Signalling.get().request_edl.emit()
        return result

    def show_alts(self, value):
        self._show_alts = value
        self.invalidateFilter()
        self.sort(0, True)

    def get_last_position(self):
        index = self.index(self.rowCount() - 1, 0)
        return index.data(ItemDataRole.ORDER_ROLE) or 0

    def filterAcceptsRow(self, source_row, source_parent):
        index = self.sourceModel().index(source_row, 0, source_parent)
        if not self._show_alts:
            return index.data(ItemDataRole.ACTIVE_ROLE)
        return True

    def rows_inserted(self, parent, first, last):
        index = self.index(first, 0)
        if index.data(ItemDataRole.TYPE_ROLE) != ItemType.INDICATOR_TYPE:
            Signalling.get().request_edl.emit()

    def rows_removed(self, parent, first, last):
        index = self.index(first, 0)
        if index.data(ItemDataRole.TYPE_ROLE) != ItemType.INDICATOR_TYPE:
            Signalling.get().request_edl.emit()

    def data_changed(self, top_left, bottom_right, roles):
        if (
            ItemDataRole.SHOT_IN_ROLE in roles
            or ItemDataRole.SHOT_OUT_ROLE in roles
            or ItemDataRole.SHOW_HANDLES_ROLE in roles
        ):
            Signalling.get().request_edl.emit()
        if QtCore.Qt.DisplayRole in roles:
            cut_order = top_left.data(ItemDataRole.CUT_ORDER_ROLE)
            if cut_order and cut_order != "-":
                Signalling.get().update_shot.emit(
                    top_left.data(),
                    top_left.data(ItemDataRole.AUDIO_OFFSET_ROLE),
                    cut_order - 1,
                )

    def clear(self):
        self._show_alts = False
