import os
import subprocess
import re

working_dir = os.path.dirname(os.path.abspath(__file__))
py_dir = os.path.dirname(working_dir)
py_dir = os.path.join(py_dir, "python")
py_dir = os.path.join(
    py_dir, [x for x in os.listdir(py_dir) if os.path.isdir(os.path.join(py_dir, x))][0]
)
py_dir = os.path.join(py_dir, "ui")

ui_files = [
    x
    for x in os.listdir(working_dir)
    if not os.path.isdir(os.path.join(working_dir, x))
    and "." in x
    and x.split(".")[-1] == "ui"
]

if "resources.qrc" in os.listdir(working_dir):

    ui_path = os.path.join(working_dir, "resources.qrc")
    py_path = os.path.join(py_dir, "resources_rc.py")

    command = "pyside6-rcc -g python -o " + py_path + " " + ui_path
    subprocess.call(command)

    data = None

    with open(py_path) as f:
        data = f.read()
    f.closed

    data = data.replace("PySide6", "tank.platform.qt")

    with open(py_path, "w") as f:
        f.write(data)
    f.closed

for ui_file in ui_files:

    ui_path = os.path.join(working_dir, ui_file)
    py_path = os.path.join(py_dir, ui_file.replace(".ui", ".py"))

    command = "pyside6-uic -g python --from-imports -o " + py_path + " " + ui_path
    subprocess.call(command)

    data = None

    with open(py_path) as f:
        data = f.read()
    f.closed

    qt_core = """from tank.platform.qt import QtCore
for name, cls in QtCore.__dict__.items():
    if isinstance(cls, type): globals()[name] = cls
"""
    qt_core_re = r"from PySide6\.QtCore import \((.|\s)+?\)"
    data = re.sub(qt_core_re, qt_core, data)

    qt_gui = """from tank.platform.qt import QtGui
for name, cls in QtGui.__dict__.items():
    if isinstance(cls, type): globals()[name] = cls
"""
    qt_gui_re = r"from PySide6\.QtGui import \((.|\s)+?\)"
    data = re.sub(qt_gui_re, qt_gui, data)
    qt_widgets_re = r"from PySide6\.QtWidgets import \((.|\s)+?\)"
    data = re.sub(qt_widgets_re, "", data)

    with open(py_path, "w") as f:
        f.write(data)
    f.closed

    # from tank.platform.qt import QtCore, QtGui
