import os
import six
import sgtk
from sgtk.platform.qt import QtCore

from locksmith.Qt import LocksmithSplash


class RvPlaylist(sgtk.platform.Application):
    APP_HOTKEY = six.b("alt+o")

    def init_app(self):
        """
        Called as the application is being initialized
        """
        self.splash = None
        self._module = None

        self._unique_panel_id = self.engine.register_panel(
            self.show_playlist_dlg,
            properties={"area": QtCore.Qt.RightDockWidgetArea},
        )

        self.engine.register_command(
            "RV Playlist",
            self.show_playlist_dlg,
            {
                "short_name": "playlist",
                "check": self.is_playlist_open,
                # dark themed icon for engines that recognize this format
                "icons": {
                    "dark": {
                        "png": os.path.join(os.path.dirname(__file__), "icon_256.png")
                    }
                },
                "hotkey": self.APP_HOTKEY,
            },
        )
        self.engine.register_command(
            "RV Playlist Open File",
            self.open_file,
            {
                "short_name": "open_file",
                "hide": True,
                # dark themed icon for engines that recognize this format
                "icons": {
                    "dark": {
                        "png": os.path.join(os.path.dirname(__file__), "icon_256.png")
                    }
                },
            },
        )

        if self.get_setting("launch_at_startup"):
            self.show_playlist_dlg()

    @property
    def module(self):
        if not self._module:
            # self.load_splash()
            # self.splash_message("Loading modules...")
            self._module = self.import_module("rv_playlist")
        return self._module

    @property
    def hotkey_data(self):
        from tank_vendor import yaml

        hotkey_path = os.path.join(os.path.dirname(__file__), "data", "hotkeys.yml")
        with open(hotkey_path, "r") as f:
            hotkey_data = yaml.safe_load(f)
        return hotkey_data

    def load_splash(self):
        self.splash = LocksmithSplash(
            self.engine._get_dialog_parent(),
            QtCore.Qt.WindowStaysOnTopHint,
            "rv playlist",
        )
        self.splash.show()

    def end_splash(self, dialog):
        if self.splash:
            dialog.__splash_screen = self.splash
            self.splash.finish(dialog.window())
            self.splash = None

    def splash_message(self, message):
        if self.splash:
            self.splash.showMessage(message)

    def show_playlist_dlg(self):
        playlist = self.module.RvPlaylist.rv_playlist()
        if playlist and playlist.dialog and playlist.dialog.isVisible():
            playlist.dialog.hide()
        else:
            self.module.RvPlaylist.show_main_dlg(self)

    def open_file(self, paths):
        playlist = self.module.RvPlaylist.rv_playlist()
        if not playlist:
            self.show_playlist_dlg()
        playlist.open_file(paths)

    def is_playlist_open(self):
        import rv.commands as rvc

        playlist = self.module.RvPlaylist.rv_playlist()
        if playlist and playlist.dialog and playlist.dialog.isVisible():
            return rvc.CheckedMenuState
        return rvc.UncheckedMenuState

    def destroy_app(self):
        """
        App teardown
        """
        self.log_debug("Destroying RvPlaylist")
        playlist = self.module.RvPlaylist.rv_playlist()
        if playlist and playlist.dialog:
            playlist.dialog.close()
            playlist.dialog.deleteLater()
        self._module = None
